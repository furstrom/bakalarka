package bakalarka.main;

import java.util.ArrayList;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import bakalarka.GraphActivity.Fragments.GraphDataThreadScanner;
import bakalarka.GraphActivity.Fragments.GraphFragment;
import bakalarka.GraphActivity.Fragments.GraphHandlerDataRender;
import bakalarka.GraphActivity.Fragments.HeatmapFragment;
import bakalarka.GraphActivity.Fragments.LegendFragment;
import bakalarka.GraphActivity.Fragments.MyFragmentPagerAdapter;
import bakalarka.myGraphView.GraphLineSeries;
import bakalarka.wifiNetworksList.AccessPoint;
import com.example.bakalarka.R;

public class MeasureActivity extends AppCompatActivity
		implements GraphFragment.OnGraphItemSelectedListener,
					HeatmapFragment.OnHeatmapItemSelectedListener,
					LegendFragment.OnLegendItemSelectedListener {

	private WifiManager mWifiManager;
	private ArrayList<AccessPoint> accessPoints; //list of APs
	private GraphDataThreadScanner dataScanner; //thread scanning wifi area
	private GraphHandlerDataRender dataRender; //thread handler processing measurued data
	private Thread scanThread; //scanning new data thread
	private ViewPager viewPager; // viewPager
	private MyFragmentPagerAdapter fragmentAdapter; //adapter for fragments
	private GraphFragment graphFragment; // fragment with GraphView
	private HeatmapFragment heatmapFragment; //fragment with HeatMapView
	private LegendFragment legendFragment; //fragment with legend
	private TabLayout tabLayout; // android TabLayout
	private Bundle extras;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		extras = getIntent().getExtras();
		setContentView(R.layout.activity_measure);
		tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
		getExtras(extras);
		mWifiManager = (WifiManager)getSystemService(WIFI_SERVICE);
		 //Get the ViewPager and set it's fragmentAdapter so that it can display items:
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);//set all three fragments alive while off screen
        fragmentAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        //create and fill fragments:
        graphFragment = new GraphFragment();
        heatmapFragment = new HeatmapFragment();
        legendFragment = new LegendFragment();
        graphFragment.setAccessPoints(accessPoints);
        heatmapFragment.setAccessPoints(accessPoints);
        legendFragment.setAccessPoints(accessPoints);
        
        //set adapter and pager:
        fragmentAdapter.addFragment(heatmapFragment, "HEATMAP");
        fragmentAdapter.addFragment(graphFragment, "GRAPH");
        fragmentAdapter.addFragment(legendFragment, "LEGEND");
        viewPager.setAdapter(fragmentAdapter);
        viewPager.setCurrentItem(1);
        
        //Give the TabLayout the ViewPager
        tabLayout.setupWithViewPager(viewPager);
        
        //thread loading new data into fragments:
        dataScanner = new GraphDataThreadScanner(mWifiManager, graphFragment.getHashMapGraphLineSeries(), graphFragment.getHashMapGraphScatterSeries());
    	scanThread = new Thread (dataScanner);
    	scanThread.start();
    	
    	//runnable handler rendering heatmapView and graphView
    	dataRender = new GraphHandlerDataRender(graphFragment, heatmapFragment);
    	dataRender.start(); 
    }
	@Override
	protected void onStart() {
		super.onStart();
		
	}
	
	
	//get extras from bundle
	private void getExtras(Bundle extras){
		accessPoints = new ArrayList<AccessPoint>();
    	ArrayList<Parcelable> parcelableArray = new ArrayList<Parcelable>();
    	parcelableArray = extras.getParcelableArrayList("APs");
    	for(int i=0;i<parcelableArray.size();i++){
    		accessPoints.add((AccessPoint)parcelableArray.get(i));
    	}   		
	}
	
	//when endActivity button is pressed
	//add extras into intent and start new activity
	public void startNextActivity(){
		Intent intent = new Intent(this, ExportActivity.class);
		ArrayList<GraphLineSeries> data = new ArrayList<GraphLineSeries>();
		data.addAll(graphFragment.getHashMapGraphLineSeries().values());
		intent.putParcelableArrayListExtra("dataSeries", data);
		startActivity(intent);		
	}
	
	public void onClickCheck(View view){
		graphFragment.switchScatterSetting();
	}
	public void showIntervals(){
		graphFragment.switchScatterSetting();
	}
	
	@Override
	protected void onPause() {
		//scanThread.interrupt();
		super.onPause();
		scanThread.interrupt();
		//mGraph.setDrawing(false);
	}	
	@Override
	protected void onResume() {		
		super.onResume();
		//if(scanThread.isInterrupted())scanThread.start();
		//mGraph.setDrawing(true);
	}
	@Override
	protected void onStop() {
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		//scanThread.interrupt();
		super.onDestroy();
		scanThread.interrupt();
	}

	@Override
	public void onGraphItemSelected(int position){
		 fragmentItemSelected(position);
		 
	 }
	@Override
	 public void onHeatmapItemSelected(int position){
		 fragmentItemSelected(position);
	 }
	
	@Override
	public void onLegendItemSelected(int position) {
		fragmentItemSelected(position);
		
	}
	//handle when item in fragment's listView is selected
	private void fragmentItemSelected(int position){
		graphFragment.clickListViewItem(position);
		heatmapFragment.clickListViewItem(position);
		legendFragment.clickListViewItem(position);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.measure_activity, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_graph_show_intervals) {
			showIntervals();
			if(item.isChecked()) item.setChecked(false);
			else item.setChecked(true);
		}
		if (id == R.id.action_graph_to_menu) {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
		}
		if (id == R.id.action_graph_csv_export) {
			startNextActivity();
		
		}
		if (id == R.id.action_graph_exit) {
			System.exit(0);
		}
		
		return super.onOptionsItemSelected(item);
	}	
}