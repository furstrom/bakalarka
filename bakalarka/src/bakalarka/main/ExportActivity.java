package bakalarka.main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import bakalarka.myGraphView.DataPoint;
import bakalarka.myGraphView.GraphLineSeries;
import com.example.bakalarka.R;
import com.opencsv.CSVWriter;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/*
 * Activity uses external library openSCV ver.3.6
 * links: http://sourceforge.net/projects/opencsv/
 * 		  http://opencsv.sourceforge.net/
 */
@SuppressLint("InflateParams")
public class ExportActivity extends AppCompatActivity {
	
	private ArrayList<GraphLineSeries> dataSeries;	
	private CSVWriter writer;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle extras = getIntent().getExtras();
		setContentView(R.layout.activity_ending);
		
		if(extras != null){
			getExtras(extras);			
		}
	}
	
	//when button is pressed
	//write data to file
	public void writeToCSV(View view) throws IOException{
				
		final View inFilename =  getLayoutInflater().inflate(R.layout.filename, null);
		//new AlertDialog.Builder(this,AlertDialog.THEME_DEVICE_DEFAULT_DARK)
		new AlertDialog.Builder(this)
		.setTitle("Zadejte nazev souboru")
		.setView(inFilename)
		.setPositiveButton("OK", 
				new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						EditText title = (EditText)inFilename.findViewById(R.id.csvname);
						final String titleFile=title.getText().toString();
						if(!titleFile.equals("")){
							createFile(titleFile);
							writeIntoFile();		
						}else{
							int time = 2* Toast.LENGTH_LONG ;
							showText("Spatny nazev souboru!", time);			
						}
					}
				})
				.setNegativeButton("Zrusit", null)
				.show();					
	}
	
	private void createFile(String name){
		File sdCard = Environment.getExternalStorageDirectory();
		File dir = new File (sdCard+ File.separator +"bakalarka");
		dir.mkdirs();
		File file  = new File(dir,name + ".csv");
		try {
			writer = new CSVWriter(new FileWriter(file),'\t');
		} catch (IOException e) {			
			e.printStackTrace();
		}		
	}
	
	private boolean writeIntoFile(){
		
		List<String[]> data = new ArrayList<String[]>();
		for(GraphLineSeries series: dataSeries){
			String[] array = dataSeriesToStringArray(series);
			data.add(array);
		}	
		
		List<String[]> data2 = new ArrayList<String[]>();
		for(int columns=0;columns<data.get(0).length;columns++){
			String[] array = new String[data.size()];
			for(int rows=0;rows<data.size();rows++){
				array[rows]=data.get(rows)[columns];
			}
			data2.add(array);
			
		}
		
		writer.writeAll(data2);
		showText("Soubor ulozen", Toast.LENGTH_LONG);
		try {
			writer.close();
			return true;
		} catch (IOException e) {
			return false;
		}	
	}
	
	private String[] dataSeriesToStringArray(GraphLineSeries series){
		List<String> list = new ArrayList<String>();
		String[] array = new String[series.size()+4];
		list.add(series.getName());
		list.add(series.getMac());
		list.add(Integer.toString(series.getFrequency()));
		list.add(series.getSecure());
		for(int i=0;i<series.size();i++){
			DataPoint point = series.get(i);
			list.add(Integer.toString(point.getValue()));
		}
		list.toArray(array);
		return array;
	}
		
	//get extras from bundle
	private void getExtras(Bundle extras){
		dataSeries = new ArrayList<GraphLineSeries>();
		ArrayList<Parcelable> parcelableArray = new ArrayList<Parcelable>();
		parcelableArray = extras.getParcelableArrayList("dataSeries");
				
    	for(int i=0;i<parcelableArray.size();i++){
    		dataSeries.add((GraphLineSeries)parcelableArray.get(i));
    	}		
	}
	
	private void showText(String text,int length){
		Toast.makeText(getApplicationContext(), text, 
				 length).show();
		
	}
	//when Menu button is pressed
	public void toMainActivity(View view){
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.export_activity, menu);
		return true;
	}

	@Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_export_to_menu) {
        	Intent intent = new Intent(this, MainActivity.class);
    		startActivity(intent);
        }
        if (id == R.id.action_export_exit) {
        	System.exit(0);
        }
        return super.onOptionsItemSelected(item);
    }
}
