package bakalarka.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import com.example.bakalarka.R;
import bakalarka.helps.NetworklistReciever;
import bakalarka.wifiNetworksList.AccessPoint;
import bakalarka.wifiNetworksList.INetworksListItem;
import bakalarka.wifiNetworksList.MyNetworkListAdapter;
import bakalarka.wifiNetworksList.NetworkName;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;
import android.net.wifi.ScanResult;

public class NetworksListActivity extends AppCompatActivity implements OnItemClickListener{
	
	private WifiManager mWifiManager ;
	private int numberOfSelected=0;
	private ListView mListView = null;
	private ArrayList<INetworksListItem> mListItems;
	private MyNetworkListAdapter mAdapter;
	/* every index 'i' is TRUE/FALSE depending on item 'i' in listView is selected or not */
	private boolean selectedItems[];
	IntentFilter intentFiter;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		init();
	}
	
	private void init(){
		setContentView(R.layout.networks_list);
		mListView = (ListView)findViewById(R.id.networksList);
		
		mWifiManager = (WifiManager)getSystemService(WIFI_SERVICE);	
		checkWifiStatus();
		
		registerReceiver(mWifiReceiver, new IntentFilter(WifiManager.RSSI_CHANGED_ACTION));
		mWifiManager.startScan();
		doScan(getCurrentFocus());		
	}
	
	//check/turnOn wifi
	private void checkWifiStatus(){
		if (mWifiManager.isWifiEnabled() == false){   
	           Toast.makeText(getApplicationContext(), "Zap�n�m WIFI", 
	           Toast.LENGTH_LONG).show();     
	           mWifiManager.setWifiEnabled(true);  	   
	    }	
	}
	
	/* make new scan via AsyncTask thread*/
	public void doScan(View view){
		
		new FindAccesPointsTask().execute(this);
		
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void onItemClick(AdapterView arg0, View arg1,int position,long arg3){	
		
		if(selectedItems[position]==false){
			selectedItems[position]=true;
			numberOfSelected++;
		} else{
			selectedItems[position]=false;
			numberOfSelected--;
		}
	}
	
	//select all items 
	public void selectAll(View view){
		for(int i=0;i< mListItems.size();i++){
			if(mListItems.get(i).isSection()==false)
			mListView.setItemChecked(i, true);
			numberOfSelected++;
		}
		Arrays.fill(selectedItems , true);
	}
	
	//select non items
	public void selectNon(View view){
		for(int i=0;i< mListItems.size();i++){
			if(mListItems.get(i).isSection()==false)
			mListView.setItemChecked(i, false);
			numberOfSelected--;
		}
		Arrays.fill(selectedItems , false);
	}
	
	/* start new activity */
	public void startListener(View view){
		
		if(numberOfSelected==0){
			 Toast.makeText(getApplicationContext(), "Vyberte s�te", 
			 Toast.LENGTH_LONG).show();
			 return;			
		}
		Intent intent = new Intent(this, MeasureActivity.class);
		intent.putParcelableArrayListExtra("APs", getSelectedMacAddress());
		startActivity(intent);
	}
	
	/* return String array of selected APs MAC address */
	public ArrayList<AccessPoint> getSelectedMacAddress(){
		ArrayList<AccessPoint> checked = new ArrayList<AccessPoint>(numberOfSelected);
		
		for(int i=0;i<mListItems.size();i++){
			
			if(selectedItems[i]){
				
				if(!mListItems.get(i).isSection() && !mListItems.get(i).isNonSelcetableItem()){	
					checked.add((AccessPoint)mListItems.get(i));
				}
			}
		}
		return checked;				
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.networklist_activity, menu);
		return true;
	}

	@Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_networklist_to_menu) {
        	Intent intent = new Intent(this, MainActivity.class);
    		startActivity(intent);
        }
        if (id == R.id.action_networklist_exit) {
        	System.exit(0);
        }
        return super.onOptionsItemSelected(item);
    }
    
	@Override
	protected void onPause(){
		super.onPause();
		unregisterReceiver(mWifiReceiver);		
		//LocalBroadcastManager.getInstance(this).unregisterReceiver(mWifiReceiver);
	}
	
	@Override
	protected void onResume() {
        super.onResume();
        //init();
        registerReceiver(mWifiReceiver, new IntentFilter(WifiManager.RSSI_CHANGED_ACTION));
    }
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		//unregisterReceiver(mWifiReceiver);
	}
	
	/* 
	 * start of subclass FindAccesPointTask
	 * scanning wifi networks and filling listView via asyncTask 
	 */
	private class FindAccesPointsTask extends AsyncTask<NetworksListActivity, Void, NetworksListActivity>{
		
		@Override
		protected void onPreExecute() {
			checkWifiStatus();
			mListItems = new ArrayList<INetworksListItem>();
			mWifiManager.startScan();			
		}
		
		@Override
		protected NetworksListActivity doInBackground(NetworksListActivity... params) {
			
			List<ScanResult> scanResults = mWifiManager.getScanResults();
			/*
			 * hashMap of networks and theirs AccesPoints 
			 * key - String = network SSID
			 * value - ArrayList<AccessPoint> = list of AccesPoints for each network(for each key) 
			 */
			final HashMap<String, ArrayList<AccessPoint>> networksHash = new HashMap<String,ArrayList<AccessPoint>>();
			ArrayList<AccessPoint> currentApList;

			for(int i=0; i<scanResults.size();i++){
				if(networksHash.containsKey(scanResults.get(i).SSID)){
					String actualNetworkName = scanResults.get(i).SSID;
					currentApList = networksHash.get(actualNetworkName);
					AccessPoint newAP = new AccessPoint(scanResults.get(i).BSSID,scanResults.get(i).frequency,scanResults.get(i).level,scanResults.get(i).SSID,scanResults.get(i).capabilities);
					currentApList.add(newAP);
				}else{
					String actualNetworkName = scanResults.get(i).SSID;
					currentApList = new ArrayList<AccessPoint>();
					AccessPoint newAP = new AccessPoint(scanResults.get(i).BSSID,scanResults.get(i).frequency,scanResults.get(i).level,scanResults.get(i).SSID,scanResults.get(i).capabilities);
					currentApList.add(newAP);
					networksHash.put(actualNetworkName, currentApList);
				}							
			}
			
			// filling listView
			if (networksHash.isEmpty()) {
				mListItems.add(new NetworkName("Nenalezeny zadne site"));
			} else {
			
				for (String key : networksHash.keySet()) {
					currentApList = networksHash.get(key);
					int count = currentApList.size();
					if(key==null || key.equals("")) key="'hidden ssid'";
					String siteName = key;
					NetworkName currNetwork = new NetworkName(siteName);
					currNetwork.setAccessPointsCount(count);
					mListItems.add(currNetwork);
					
					for(AccessPoint ap : currentApList){
						mListItems.add(ap);	
						
					}
				}		
			}
			
			/* Inicialization of selectedItems array to 'false' values */
			selectedItems = new boolean[mListItems.size()];
			Arrays.fill(selectedItems , false);
			
			return params[0];
		}
			
		@Override
		protected void onPostExecute(NetworksListActivity result) {
			mAdapter = new MyNetworkListAdapter(result, mListItems);
			mListView.setAdapter(mAdapter);
			mListView.setOnItemClickListener(result);
	     }
				
	}//end of FIndAccesPointTask subclass
	
	//start broadcast Receiver
	private BroadcastReceiver mWifiReceiver =new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
        	
        	NetworklistReciever.recieveNewData(mWifiManager, mListItems, mAdapter);            
        }
    };//end broadcast recEIver	
    
} //end of NetworksList class