package bakalarka.main;

import com.example.bakalarka.R;
import bakalarka.main.NetworksListActivity;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {
		
	//spusti novou aktivitu pro pripojeni k siti
	public void selectNetworkActivity(View view){
		Intent intent = new Intent(this, NetworksListActivity.class);
		startActivity(intent);		
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		if (id == R.id.main_activity_exit) {
			System.exit(0);
		}

		return super.onOptionsItemSelected(item);
	}
}
