package bakalarka.helps;

import java.util.ArrayList;
import java.util.List;

import bakalarka.wifiNetworksList.AccessPoint;
import bakalarka.wifiNetworksList.INetworksListItem;
import bakalarka.wifiNetworksList.MyNetworkListAdapter;
import bakalarka.wifiNetworksList.NonSelectableAccessPoint;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;


/*
 * static class making receiver work from NetWorkListActivity class 
 * 
 *  */

public final class NetworklistReciever {
	
	private NetworklistReciever(){
		
	}
		
	/*
	 * @param wifiManager - from NetWorkListActivity class 
	 * @param listItem - from NetWorkListActivity class
	 * @param adapter - from NetWorkListActivity class
	 * 
	 * compare old items in listView with new items in scanResult and change AP to nonAP or back
	 *
	 * */
	
	public static final void recieveNewData(WifiManager wifiManager,ArrayList<INetworksListItem> listItems,MyNetworkListAdapter adapter){
		boolean doContinue=false;
    	List<ScanResult> scanResults = wifiManager.getScanResults();
		//go throw all items in lisItems
		for(int i=0;i<listItems.size();i++){
			doContinue=false;
			if(listItems.get(i).isSection()) continue;
			//go throw all results in scanResults
			for(int j=0;j<scanResults.size();j++){	
				if(listItems.get(i).getMacAddress().equals(scanResults.get(j).BSSID)){
					if(listItems.get(i).isNonSelcetableItem()){
						INetworksListItem item = listItems.get(i);;
						AccessPoint AP =  item.toAccessPoint();
						listItems.set(i, AP);	
					}
					doContinue=true;
					break;
					
				}		
			}
			if(doContinue==true) continue;
			INetworksListItem item = listItems.get(i);
			NonSelectableAccessPoint nonAP =  item.toNonSelectableAccessPoint();
			listItems.set(i, nonAP);
			adapter.notifyDataSetChanged();
    	
		}
		
	}

}
