package bakalarka.helps;

import bakalarka.myGraphView.GraphView;
import bakalarka.myHeatMapView.HeatMapView;
import android.content.res.Configuration;
import android.widget.LinearLayout;
import android.widget.ListView;

public class RotationChangeHandler {
	
	public static void graph(Configuration newConfig,GraphView graph ,ListView textGraphLegend){
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			LinearLayout.LayoutParams graphParams = new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, 0, 9.0f);
			LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, 0, 0.0f);
			
			graph.setLayoutParams(graphParams);
			textGraphLegend.setLayoutParams(textParams);	
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
        	LinearLayout.LayoutParams graphParams = new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT,0, 6.0f);
        	LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, 0, 4.0f);

        	graph.setLayoutParams(graphParams);
			textGraphLegend.setLayoutParams(textParams);		
        }		
	}
	public static void graph(GraphView graph ,ListView textGraphLegend){
		
			LinearLayout.LayoutParams graphParams = new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, 0, 9.0f);
			LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, 0, 0.0f);
			
			graph.setLayoutParams(graphParams);
			textGraphLegend.setLayoutParams(textParams);	
        
	}
	
	public static void heatMap(Configuration newConfig,HeatMapView map ,ListView textMapLegend){
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			LinearLayout.LayoutParams mapParams = new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, 0, 9.0f);
			LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, 0, 0.0f);
		

			map.setLayoutParams(mapParams);	
			textMapLegend.setLayoutParams(textParams);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
        	LinearLayout.LayoutParams mapParams = new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT,0, 6.0f);
        	LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, 0, 4.0f);

        	map.setLayoutParams(mapParams);
        	textMapLegend.setLayoutParams(textParams);		
        }
	}
	public static void heatMap(HeatMapView map ,ListView textMapLegend){
			LinearLayout.LayoutParams mapParams = new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, 0, 9.0f);
			LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, 0, 0.0f);
		
			map.setLayoutParams(mapParams);	
			textMapLegend.setLayoutParams(textParams);
     } 

}
