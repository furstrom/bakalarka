package bakalarka.helps;

public final class MyMath {
	
	public static int euclidSize(float firstX,float firstY,float secondX, float secondY){
		float diffX = firstX-secondX;
		float diffY = firstY- secondY;
		return (int)Math.sqrt(Math.pow(diffX, 2)+Math.pow(diffY, 2));
		
	}

}
