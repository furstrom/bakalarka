package bakalarka.helps;

import android.annotation.SuppressLint;

public final class Convert {
	
	@SuppressLint("DefaultLocale")
	public static String getTimeFormat(double dValue){

		String sec="0";
		String returnVal;
		String min;
	
		if(dValue<60){
			if(dValue<10){ 
				//sec=Double.toString(dValue);
				sec=String.format("%.1f", dValue);
				returnVal="0:0"+sec;
				return  returnVal;
			}		
			//sec=Double.toString(dValue);
			sec=String.format("%.1f", dValue);
			returnVal="0:"+sec;
			return returnVal;
						
		}
		int minutes = (int)dValue/60;
		
		if(minutes>60){
			int hours=minutes/60;
			minutes=minutes-(hours*60);
			}
		
		dValue=dValue-(minutes*60);
		//sec=Double.toString(dValue);
		sec=String.format("%.1f", dValue);
		if(dValue<10)sec="0"+sec;
		min=Integer.toString(minutes);
		if(minutes<10)min="0"+min;
		returnVal=min+":"+sec;
		
		return returnVal;
	}
	public static String getTimeFormat(int iValue){
		String sec="0";
		String returnVal="";
		String min;
		
		if(iValue<60){
			if(iValue<10){ 
				sec=Integer.toString(iValue);
				returnVal="0:0"+sec;
				return  returnVal;
			}		
			sec=Integer.toString(iValue);
			returnVal="0:"+sec;
			return returnVal;
		}
		int minutes = iValue/60;
		
		if(minutes>60){
			int hours=minutes/60;
			minutes=minutes-(hours*60);
			
		}
		
		iValue=iValue-(minutes*60);
		sec=Integer.toString(iValue);
		if(iValue<10)sec="0"+sec;
		min=Integer.toString(minutes);
		if(minutes<10)min="0"+min;
		returnVal=min+":"+sec;
		
		return returnVal;
	}
}
