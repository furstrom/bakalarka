package bakalarka.helps;

import android.graphics.Color;

public final class MyColors {
	
	private static int colors[] = {
	//DARK_BLUE = 
	Color.parseColor("#0077ff"),
	//int DARK_RED = 
	Color.parseColor("#ff0000"),
	//DARK_GREEN = 
	Color.parseColor("#008000"),
	//DARK_PINK = 
	Color.parseColor("#ff00ff"),
	//YELLOW = 
	Color.parseColor("#ffff00"),
	//TYRKYS = 
	Color.parseColor("#00ffff"),
	//BROWN = 
	Color.parseColor("#8b4513"),
	// GREY = 
	Color.parseColor("#808080"),
	//HARLEQUIN = 
	Color.parseColor("#00ff00"),
	//ORANGE = 
	Color.parseColor("#ffa500"),
	//WHITE = 
	Color.parseColor("#fffff0"),					
	//LIGHT_BLUE = 
	Color.parseColor("#1e90ff"),
	//LIGHT_RED = 
	Color.parseColor("#ff6347"),
	//LIGHT_GREEN = 
	Color.parseColor("#3cb371"),
	//LIGHT_PINK = 
	Color.parseColor("#ffb6c1"),
	};
	
	public static int get(int i){
		if(i>=colors.length) return colors[i%getCount()];
		return colors[i];
	}
	
	public static int getCount(){
		return colors.length;		
	}
	
	public static int getIntFromColor(float red, float green, float blue){
	    int r = Math.round(255 * red);
	    int g = Math.round(255 * green);
	    int b = Math.round(255 * blue);

	    r = (r << 16) & 0x00FF0000;
	    g = (g << 8) & 0x0000FF00;
	    b = b & 0x000000FF;

	    return 0xFF000000 | r | g | b;
	}	
	

}
