package bakalarka.wifiNetworksList;

/*
 * trida NetworkName reprezentuje nazev site (nazev sekce v listView)
 * v nasem propade jde o SSID site 
*/

public class NetworkName implements INetworksListItem{
	
	//nazev sekce polozek v listView
	private final String name;
	//pocet ap pro danou sit
	private int accessPointsCount;
	
	@Override
	public String getMacAddress(){
		return null;
		
	}
	public NetworkName(String name){
		this.name=name;
	}
	
	public String getName(){
		return name;
	}
	
	public int getAccessPointsCount() {
		return accessPointsCount;
	}
	public void setAccessPointsCount(int accessPointsCount) {
		this.accessPointsCount = accessPointsCount;
	}
	@Override
	public boolean isSection() {
		return true;
	}
	@Override
	public boolean isNonSelcetableItem() {
		return false;
	}
	@Override
	public AccessPoint toAccessPoint() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public NonSelectableAccessPoint toNonSelectableAccessPoint() {
		// TODO Auto-generated method stub
		return null;
	}

}
