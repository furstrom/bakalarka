package bakalarka.wifiNetworksList;

public interface INetworksListItem {
	public boolean isSection();
	public boolean isNonSelcetableItem();
	public String getMacAddress();
	public AccessPoint toAccessPoint();
	public NonSelectableAccessPoint toNonSelectableAccessPoint();

}
