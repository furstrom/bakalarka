package bakalarka.wifiNetworksList;

public class NonSelectableAccessPoint implements INetworksListItem{
	private final String macAddress;
	private int frequency; 
	private int level;
	private String network;
	private boolean selected;
	private String security;
	private boolean secured;
	
	public NonSelectableAccessPoint(){	
		this.macAddress = null;	
		this.frequency = 0;
		this.level = 0;
		this.network = null;
		this.selected = false;
		this.secured = false;
		this.security="FREE";
	}

	public NonSelectableAccessPoint(String macAddress,int frequency,int level,String network,String capabilities){
		this.macAddress = macAddress;
		this.frequency = frequency;
		this.level = level;
		this.network = network;
		this.selected = false;
		setSecure(capabilities);
	}
	
	@Override
	public String getMacAddress(){
		return macAddress;
	}
	
	public int getLevel(){
		return level;
	}
	
	public int getFrequency(){
		return frequency;
	}
	
	public String getNetwork(){
		return network;
	}

	@Override
	public boolean isSection() {
		return false;
	}
	
	public boolean isSelected(){
		return selected;	
	}
	
	public void setSelected(boolean selected){
		this.selected=selected;
	}
	
	public boolean isSecured(){
		return secured;
	}
	
	public String getSecure(){
		return security;
	}
	
	private void setSecure(String capabilities){
		if(capabilities.contains("WEP")){
			this.security = "WEP";
		}else if(capabilities.contains("WPA2-PSK")){
			this.security = "WPA2-PSK";
		}else if(capabilities.contains("WPA2")){
			this.security = "WPA2";
		}else if(capabilities.contains("WPA")){
			this.security = "WPA";
		}else{
			this.security = "FREE";
			this.secured = false;
			return;
		}
		this.secured = true;
	}
	
	@Override
	public boolean isNonSelcetableItem() {
		return true;
	}

	@Override
	public AccessPoint toAccessPoint() {
		return new AccessPoint(macAddress, frequency, level, network,security);
	}

	@Override
	public NonSelectableAccessPoint toNonSelectableAccessPoint() {
		return this;
	}


}
