package bakalarka.wifiNetworksList;

import android.os.Parcel;
import android.os.Parcelable;

/*
 * Trida AccessPoint reprezentuje jednu polozku v listView (jednotlive polozky dane sekce)
 * V nasem pripade jde o jednotlive AP pro konkretni sit
 */

public class AccessPoint implements INetworksListItem ,Parcelable{
	
	private final String macAddress;
	private int frequency; 
	private int level;
	private String network;
	private boolean selected;
	private String security;
	private boolean secured;
	
	public AccessPoint(){	
		this.macAddress = null;
		this.frequency = 0;
		this.level = 0;
		this.network = null;
		this.selected = false;
		this.secured = false;
		this.security="FREE";
	}
	
	public AccessPoint(String macAddress,int frequency,int level,String network,String capabilities){
		this.macAddress = macAddress;
		this.frequency = frequency;
		this.level = level;
		this.network = network;
		this.selected = false;
		setSecure(capabilities);
	
	}
		
	/*
	 * Constructor to use when re-constructing object
	 * from a parcel
	 * 
	 * @param in a parcel from which to read this object
	 * */
	public AccessPoint(Parcel in){
		this.macAddress = in.readString();
		this.frequency = in.readInt();
		this.level = in.readInt();
		this.network = in.readString();
		this.selected = false;
		this.security = in.readString();
		if(security.contains("FREE")) this.secured = false;
		else this.secured = true;	
	}
	
	@Override
	public String getMacAddress(){
		return macAddress;
	}
	
	public int getLevel(){
		return level;
	}
	
	public int getFrequency(){
		return frequency;
	}
	
	public String getNetwork(){
		return network;	
	}

	@Override
	public boolean isSection() {
		return false;
	}
	
	public boolean isSelected(){
		return selected;	
	}
	
	
	public void setSelected(boolean selected){
		this.selected=selected;
	}
	
	public boolean isSecured(){
		return secured;
	}
	
	public String getSecure(){
		return security;
	}
	
	private void setSecure(String capabilities){
		if(capabilities.contains("WEP")){
			this.security = "WEP";
		}else if(capabilities.contains("WPA2-PSK")){
			this.security = "WPA2-PSK";
		}else if(capabilities.contains("WPA2")){
			this.security = "WPA2";
		}else if(capabilities.contains("WPA")){
			this.security = "WPA";
		}else{
			this.security = "FREE";
			this.secured = false;
			return;
		}
		this.secured = true;
	}

	@Override
	public boolean isNonSelcetableItem() {
		return false;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(macAddress);
		dest.writeInt(frequency);
		dest.writeInt(level);
		dest.writeString(network);
		dest.writeString(security);
	}
	
	/*
	 * This field is needed for Android to be able to
	 * create new objects, individually or as arrays 
	 */
	public static final Parcelable.Creator<AccessPoint> CREATOR = 
			new Parcelable.Creator<AccessPoint>() {
				@Override
				public AccessPoint createFromParcel(Parcel in) {
					return new AccessPoint(in);
				}	
				@Override
				public AccessPoint[] newArray(int size) {
					return new AccessPoint[size];
				}
	};

	@Override
	public AccessPoint toAccessPoint() {
		return this;
	}

	@Override
	public NonSelectableAccessPoint toNonSelectableAccessPoint() {
		// TODO Auto-generated method stub
		return new NonSelectableAccessPoint(macAddress, frequency, level, network,security);
	}
				

}
