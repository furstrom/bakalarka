package bakalarka.wifiNetworksList;

import java.util.ArrayList;

import com.example.bakalarka.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MyNetworkListAdapter extends ArrayAdapter<INetworksListItem>{
	
	private ArrayList<INetworksListItem> mListItems;
	private LayoutInflater mLayoutInflater;
	
	public MyNetworkListAdapter(Context context,ArrayList<INetworksListItem> items){
		super(context,0,items);
	
		this.mListItems = items;	
		this.mLayoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);		
	}	
		
	@Override
	public int getCount() {
		return mListItems.size();
	}
	
	@Override
	public INetworksListItem getItem(int position) {
		return mListItems.get(position);
	}
	
	@SuppressLint("InflateParams")
	@Override
	public View getView(int position,View convertView,ViewGroup parent){
		View view = convertView;
		
		final INetworksListItem listItem = mListItems.get(position);
		if(listItem != null){
			if(listItem.isSection()){
				NetworkName networkName = (NetworkName) listItem;
				view = mLayoutInflater.inflate(R.layout.list_item_network_name , null);
				
				view.setOnClickListener(null);
				view.setOnLongClickListener(null);
				view.setLongClickable(false);
				
				final TextView networkNameView = (TextView) view.findViewById(R.id.list_item_network_name);
				final TextView accessPointsCount = (TextView) view.findViewById(R.id.list_item_network_count); 
				networkNameView.setText(networkName.getName());	
				int count = networkName.getAccessPointsCount();
				accessPointsCount.setText(Integer.toString(count));
				
			}else if(listItem.isNonSelcetableItem()){
				NonSelectableAccessPoint nonSelectAP = (NonSelectableAccessPoint) listItem;
				view = mLayoutInflater.inflate(R.layout.list_item_nonselectable_ap, null);
				
				view.setOnClickListener(null);
				view.setOnLongClickListener(null);
				view.setLongClickable(false);
				
				final TextView title = (TextView) view.findViewById(R.id.list_item_nonselect_ap_mac);
				final TextView 	subtitle = (TextView) view.findViewById(R.id.list_item_nonselect_ap_info);
				
				if(title != null)
					title.setText(nonSelectAP.getMacAddress());
				if(subtitle != null){
					String frequency = Double.toString((double)nonSelectAP.getFrequency()/1000);
					String level = Integer.toString(nonSelectAP.getLevel());
					String secure = nonSelectAP.getSecure();
					subtitle.setText(frequency+"GHz, " + level + "dB, " + secure);
					}
				
				
			}else{
				AccessPoint accessPoint = (AccessPoint) listItem;
				view = mLayoutInflater.inflate(R.layout.list_item_ap , null);
				
				final TextView title = (TextView) view.findViewById(R.id.list_item_ap_mac);
				final TextView 	subtitle = (TextView) view.findViewById(R.id.list_item_ap_info);
				
				if(title != null)
					title.setText(accessPoint.getMacAddress());
				if(subtitle != null){
					String frequency = Double.toString((double)accessPoint.getFrequency()/1000);
					String level = Integer.toString(accessPoint.getLevel());
					String secure = accessPoint.getSecure();
					subtitle.setText(frequency+"GHz, " + level + "dB, " + secure);
					}
			}
		}
		return view;		
	}
}
