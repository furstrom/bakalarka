package bakalarka.myHeatMapView;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

class MapAxis {
	
	private Canvas canvas;
	private Point leftUp;
	private Point leftDown;
	private Point rightDown;
	private int mapWidth;
	//private float strokeWidth;
	private int color;
	private final float LINE_WIDTH;
	
	public MapAxis(Canvas canvas,MapParams mapParams){
		this.canvas = canvas;

		// set to default values
		leftUp = mapParams.getLeftUpCorner();
		leftDown = mapParams.getLeftDownCorner();
		rightDown = mapParams.getRightDownCorner();
		mapWidth = mapParams.getMapWidth();
		LINE_WIDTH = mapParams.getAxisLineWidth();
		color = Color.WHITE;
	}
	
	public void draw(){
		Paint axis = new Paint();
		axis.setColor(Color.WHITE);
		axis.setStrokeWidth(LINE_WIDTH);
		canvas.drawLine(leftUp.x, leftUp.y, leftDown.x, leftDown.y, axis);
		canvas.drawLine(leftDown.x, leftDown.y, rightDown.x, rightDown.y, axis);
		canvas.drawLine(leftDown.x+mapWidth/4, leftDown.y, leftDown.x+mapWidth/4, leftDown.y+8, axis);
		canvas.drawLine(leftDown.x+mapWidth/2, leftDown.y, leftDown.x+mapWidth/2, leftDown.y+8, axis);
		canvas.drawLine(leftDown.x+mapWidth*3/4, leftDown.y, leftDown.x+mapWidth*3/4, leftDown.y+8, axis);
	}

	public Canvas getCanvas() {
		return canvas;
	}

	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
	}

	public Point getLeftUp() {
		return leftUp;
	}

	public void setLeftUp(Point leftUp) {
		this.leftUp = leftUp;
	}

	public Point getLeftDown() {
		return leftDown;
	}

	public void setLeftDown(Point leftDown) {
		this.leftDown = leftDown;
	}

	public Point getRightDown() {
		return rightDown;
	}

	public void setRightDown(Point rightDown) {
		this.rightDown = rightDown;
	}

	public int getMapWidth() {
		return mapWidth;
	}

	public void setMapWidth(int mapWidth) {
		this.mapWidth = mapWidth;
	}

	public float getStrokeWidth() {
		return LINE_WIDTH;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}
	
	

}
