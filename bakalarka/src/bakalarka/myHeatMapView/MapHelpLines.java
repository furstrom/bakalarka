package bakalarka.myHeatMapView;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

class MapHelpLines {
	
	private Canvas canvas;
	private MapParams params;
	private Point leftUp;
	private int mapWidth;
	private int networksNumber;
	
	
	public MapHelpLines(Canvas canvas, MapParams params){
		this.canvas=canvas;
		this.params=params;
		this.leftUp=params.getLeftUpCorner();
		this.mapWidth = params.getMapWidth();
		
	} 
	
	public void draw(){
		//this.networksNumber = params.getNetworkNumber();
		this.networksNumber = params.getNumberOfVisibleAp();
		Paint line = new Paint();
		line.setColor(Color.WHITE);
		line.setStrokeWidth(2);
		int x = leftUp.x-8;
		int y = leftUp.y;
		float size=(float)params.getMaphHeight()/(float)networksNumber;
		
		for(int i=1;i<=networksNumber;i++){
			canvas.drawLine(x, y+(i*size), leftUp.x+mapWidth, y+(i*size), line);			
			
		}		
	}

}
