package bakalarka.myHeatMapView;

import java.util.LinkedList;

class ColorGradient {
	
	 class ColorPoint  // Internal class used to store colors at different points in the gradient.
	  {
	    float r,g,b;      // Red, green and blue values of our color.
	    float val;        // Position of our color along the gradient (between 0 and 1).
	    ColorPoint(float red, float green, float blue, float value){
	    	r=red;
	    	g=green;
	    	b=blue;
	    	val=value;
	    }
	  };
	  
	  private LinkedList<ColorPoint> color = new LinkedList<ColorPoint>();
	  
	  public ColorGradient(){
		  createDefaultHeatMapGradient();
	  }
	  private void createDefaultHeatMapGradient(){
	    //color.clear();
	    color.addLast(new ColorPoint(0, 0, 1,   0.0f));      // Blue.
	    color.addLast(new ColorPoint(0, 1, 1,   0.25f));     // Cyan.
	    color.addLast(new ColorPoint(0, 1, 0,   0.5f));      // Green.
	    color.addLast(new ColorPoint(1, 1, 0,   0.75f));     // Yellow.
	    color.addLast(new ColorPoint(1, 0, 0,   1.0f));      // Red.
	  }
	  
	  //-- Inputs a (value) between 0 and 1 
	  //-- values representing that position in the gradient.
	  //-- return array rgb float values
	  float[] getColorAtValue(float value){
	  float rgb[]= {0,0,0};
	  
	    if(color.size()==0)
	      return rgb;
	 
	    for(int i=0; i<color.size(); i++)
	    {
	      ColorPoint currColor = color.get(i);
	      if(value < currColor.val) {
	        ColorPoint prevColor  = color.get(Math.max(0,i-1));
	        float valueDiff    = (prevColor.val - currColor.val);
	        float fractBetween = (valueDiff==0) ? 0 : (value - currColor.val) / valueDiff;
	        rgb[0]   = (prevColor.r - currColor.r)*fractBetween + currColor.r;
	        rgb[1] = (prevColor.g - currColor.g)*fractBetween + currColor.g;
	        rgb[2]  = (prevColor.b - currColor.b)*fractBetween + currColor.b;
	        return rgb;
	      }
	    }
	    rgb[0]   = color.getLast().r;
	    rgb[1] = color.getLast().g;
	    rgb[2]  = color.getLast().b;
	    return rgb;
	  }

}
