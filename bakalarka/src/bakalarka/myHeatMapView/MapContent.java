package bakalarka.myHeatMapView;

import java.util.ArrayList;
import java.util.List;
import android.graphics.Canvas;
import bakalarka.myGraphView.GraphLineSeries;

//representing content of our heat map 
//drawing heat frames
public class MapContent {
	
	private List<GraphLineSeries> pointSeries;
	private List<HeatStripe> heatStripes = new ArrayList<HeatStripe>();
	Canvas canvas;
	ColorGradient heatMapGradient;
	MapParams mapParmas;
	
	public MapContent(List<GraphLineSeries> series){
		this.pointSeries=series;
		init();
				
	}
	
	// filling heatStripes array 
	private void init(){
		if(pointSeries==null || pointSeries.isEmpty()) return;
		for(GraphLineSeries series : pointSeries){
			HeatStripe hS = new HeatStripe(series);
			heatStripes.add(hS);
		} 
	}
	
	//draw heatStripe for each data series
	public void draw(Canvas canvas, MapParams mapParams){
		int lineWidth=(int)mapParams.getAxisLineWidth();
		int nmbrOfSeries = mapParams.getNumberOfVisibleAp();
		float frameHeight = ((float)mapParams.getMaphHeight())/(float)nmbrOfSeries; //height of one frame contains in heatMap
		int x=mapParams.getLeftUpCorner().x+lineWidth;
		float y=mapParams.getLeftUpCorner().y;
		int visible = 0;//counter of visible APs in map
		
		for(int i=0;i<heatStripes.size();i++){
			HeatStripe actual = heatStripes.get(i);
			if(actual.isVisible()==false) continue;
			actual.draw(x, y, canvas, mapParams);
			y = mapParams.getLeftUpCorner().y + (visible + 1) * frameHeight;
			visible++;
		}		
	}
}
