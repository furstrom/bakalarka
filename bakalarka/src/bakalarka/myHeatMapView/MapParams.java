package bakalarka.myHeatMapView;

import android.graphics.Canvas;
import android.graphics.Point;

class MapParams {
	
	Canvas canvas;

	// four corners making a graph
	private Point leftUpCorner = new Point(70, 70);
	private Point leftDownCorner;
	private Point rightUpCorner;
	private Point rightDownCorner;

	// help values
	private int canvasWidth;
	private int canvasHeight;
	private int mapWidth;
	private int maphHeight;
	private float axisLineWidth;
	//how many networks are currently visible 
	private int numberOfVisibleAp;
	//how many networks we are measuring
	private int networksNumber;
	//instance of color gradient
	private ColorGradient colorGradient;
	//maximal frames count in heat map
	private int maxFrameCount;
	
	public MapParams(){
		axisLineWidth=3;
		
	}
	
	public void set(Canvas canvas){
		this.canvas=canvas;
		
		// set defaut values
		canvasWidth = canvas.getWidth();
		canvasHeight = canvas.getHeight();

		leftUpCorner = new Point(70, 70);
		leftDownCorner = new Point(70, canvasHeight - 70);
		rightUpCorner = new Point(canvasWidth - 50, 70);
		rightDownCorner = new Point(canvasWidth - 50, canvasHeight - 70);

		mapWidth = rightDownCorner.x - leftDownCorner.x;
		maphHeight = leftDownCorner.y - leftUpCorner.y;
	}

	public Canvas getCanvas() {
		return canvas;
	}

	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
	}

	public Point getLeftUpCorner() {
		return leftUpCorner;
	}

	public void setLeftUpCorner(Point leftUpCorner) {
		this.leftUpCorner = leftUpCorner;
	}

	public Point getLeftDownCorner() {
		return leftDownCorner;
	}

	public void setLeftDownCorner(Point leftDownCorner) {
		this.leftDownCorner = leftDownCorner;
	}

	public Point getRightUpCorner() {
		return rightUpCorner;
	}

	public void setRightUpCorner(Point rightUpCorner) {
		this.rightUpCorner = rightUpCorner;
	}

	public Point getRightDownCorner() {
		return rightDownCorner;
	}

	public void setRightDownCorner(Point rightDownCorner) {
		this.rightDownCorner = rightDownCorner;
	}

	public int getCanvasWidth() {
		return canvasWidth;
	}

	public void setCanvasWidth(int canvasWidth) {
		this.canvasWidth = canvasWidth;
	}

	public int getCanvasHeight() {
		return canvasHeight;
	}

	public void setCanvasHeight(int canvasHeight) {
		this.canvasHeight = canvasHeight;
	}

	public int getMapWidth() {
		return mapWidth;
	}

	public void setMapWidth(int mapWidth) {
		this.mapWidth = mapWidth;
	}

	public int getMaphHeight() {
		return maphHeight;
	}

	public void setMaphHeight(int maphHeight) {
		this.maphHeight = maphHeight;
	}
	
	public void setNetworksNumber(int i){
		this.networksNumber=i;
	}
	public int getNetworkNumber(){
		return networksNumber;
	}
	public float getAxisLineWidth(){
		return axisLineWidth;
	}

	public ColorGradient getColorGradient() {
		return colorGradient;
	}

	public void setColorGradient(ColorGradient colorGradient) {
		this.colorGradient = colorGradient;
	}

	public int getMaxFrameCount() {
		return maxFrameCount;
	}

	public void setMaxFrameCount(int maxFrameCount) {
		this.maxFrameCount = maxFrameCount;
	}

	public int getNumberOfVisibleAp() {
		return numberOfVisibleAp;
	}

	public void setNumberOfVisibleAp(int numberOfVisibleAp) {
		this.numberOfVisibleAp = numberOfVisibleAp;
	}		
}
