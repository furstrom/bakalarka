package bakalarka.myHeatMapView;

import java.util.List;

import bakalarka.myGraphView.GraphLineSeries;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

public class HeatMapView extends View{
	
	private MapParams mapParams=null; //parameters of HeatMap
	List<GraphLineSeries> pointSeries; //dataPoint in HeatMap
	private MapAxis mapAxis;
	private MapHelpLines mapHelpLines;
	private MapScaleX mapScaleX;
	private MapScaleY mapScaleY;
	private MapContent mapContent;
	private ColorGradient heatMapGradient = new ColorGradient(); // color gradient

	public HeatMapView(Context context) {
		super(context);
		init();
	}
	
	public HeatMapView(Context context, AttributeSet attrs){
		super(context,attrs);
		init();
	}
	
	private void init(){
		mapParams = new MapParams();
		mapParams.setColorGradient(heatMapGradient);
		pointSeries=null;		
	}
	
	@Override
	protected void onDraw(Canvas canvas){
		super.onDraw(canvas);
		
		drawHeatMapSheet(canvas);
		if(pointSeries==null)
		return;
		
		if(!pointSeries.isEmpty())
			//return;
		
		if(pointSeries!=null && !pointSeries.isEmpty()){
			mapParams.setNetworksNumber(pointSeries.size());
			
			if(mapParams.getNumberOfVisibleAp()>0){
				mapContent.draw(canvas, mapParams);
				//mapScaleX.draw();
				mapScaleY.draw(pointSeries);
			}
			
			setXScale();
			mapScaleX.draw();
			
			//set Y scale
			if(pointSeries.size()>1){
				mapHelpLines.draw();
			}
		}		
	}
	
	//draw axis a scales
	private void drawHeatMapSheet(Canvas canvas){
		mapParams.set(canvas);
		
		mapAxis = new MapAxis(canvas, mapParams);
		mapScaleX = new MapScaleX(canvas,mapParams);
		mapScaleY = new MapScaleY(canvas,mapParams);
		mapHelpLines = new MapHelpLines(canvas, mapParams);
		
		mapAxis.draw();	
	}
	
	//setting of x scale
	private void setXScale(){
		
		int dataLenght = pointSeries.get(0).size();
		mapScaleX.setScale(dataLenght);
	}
		
	public void setPointSeries(List<GraphLineSeries> pointSeries){
		synchronized (this) {
			this.pointSeries = pointSeries;
			this.mapContent = new MapContent(pointSeries);
		}
		
	}
	
	//setting of number of visible APs in heatmap
	public void setNmbrVisibleAp(int nmbr){
		mapParams.setNumberOfVisibleAp(nmbr);
		this.invalidate();
		
	}
	
	public void redraw(){
		this.invalidate();
	}
	
	public int getMaxFrameCount(){
		return mapParams.getMaxFrameCount();
	}
}
