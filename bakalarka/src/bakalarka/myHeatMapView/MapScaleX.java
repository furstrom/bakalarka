package bakalarka.myHeatMapView;

import bakalarka.helps.Convert;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

class MapScaleX {
	Canvas canvas;
	MapParams params;
	Point leftDown;
	Point rightDown;
	int mapWidth;
	// scaleX values
	String start = "0";
	String q1 = "";
	String medium = "";
	String q3 ="";
	String end = "";
	
	
	public MapScaleX(Canvas canvas,MapParams params){
		this.canvas = canvas;
		this.params = params;
		leftDown=params.getLeftDownCorner();
		rightDown=params.getRightDownCorner();
		mapWidth = params.getMapWidth();
		
	}
	
	public void draw(){
		Paint nmbrs = new Paint();
		nmbrs.setColor(Color.WHITE);
		int textSize=40;
		nmbrs.setTextSize(textSize);
		
		canvas.drawText(start, leftDown.x-10, leftDown.y+textSize+10, nmbrs);
		canvas.drawText(q1, leftDown.x+(mapWidth/4)-40, leftDown.y+textSize+10, nmbrs);
		canvas.drawText(medium, leftDown.x+(mapWidth/2)-40, leftDown.y+textSize+10, nmbrs);
		canvas.drawText(q3, leftDown.x+(mapWidth*3/4)-40, leftDown.y+textSize+10, nmbrs);
		canvas.drawText(end, rightDown.x-45, rightDown.y+textSize+10, nmbrs);
		
	}
	
	public void setScale(int dataLenght){
		
		// computing time
		int timeEnd = dataLenght / 20; // time in seconds
		double timeQ1 = dataLenght / 4;
		timeQ1 = timeQ1 / 20; // quarter time in seconds
		double timeMid = (dataLenght) / 2;
		timeMid = timeMid / 20; // half time in seconds
		double timeQ3 = (dataLenght) * 3 / 4;
		timeQ3 = timeQ3 / 20; // three quarter time in seconds

		// converting to String
		String sTimeQ1 = Convert.getTimeFormat(timeQ1);
		String sTimeMid = Convert.getTimeFormat(timeMid);
		String sTimeQ3 = Convert.getTimeFormat(timeQ3);
		String sTimeEnd = Convert.getTimeFormat(timeEnd);

		// setting values
		setValue(2, sTimeQ1);
		setValue(3, sTimeMid);
		setValue(4, sTimeQ3);
		setValue(5, sTimeEnd);	
	}
	
	
	public void setValue(int i,String value){
		switch (i) {
		case 1:
			start=value;
			break;
		case 2:
			q1=value;
			break;	
		case 3:
			medium=value;
			break;	
		case 4:
			q3=value;
			break;	
		case 5:
			end=value;
			break;
			

		default:
			break;
		
		}
	}

}
