package bakalarka.myHeatMapView;

import java.util.List;

import bakalarka.myGraphView.GraphLineSeries;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;

class MapScaleY {
	private Canvas canvas;
	private MapParams params;
	private int VisibleNetworksNumber;
	private int textsize;
	
	public MapScaleY(Canvas canvas, MapParams params){
		this.canvas=canvas;
		this.params=params;	
		this.textsize=25;
	}
	
	public void draw(List<GraphLineSeries> pointSeries){
		int networksNumber = params.getNetworkNumber();
		this.VisibleNetworksNumber = params.getNumberOfVisibleAp();
		Paint rect = new Paint();
		Paint text = new Paint();
		int size=params.getMaphHeight()/VisibleNetworksNumber;
		int left=20;
		int top = params.getLeftUpCorner().y+15;
		int right = params.getLeftUpCorner().x-15;
		int bottom = params.getLeftUpCorner().y+size-15;
		
		rect.setStyle(Paint.Style.FILL);
		text.setTextSize(textsize);
		text.setStrokeWidth(3);
		text.setTextAlign(Align.CENTER);
		int counter=0;
		canvas.drawRect(left, top,right,bottom,rect);
		for(int i=0;i<networksNumber;i++){
			GraphLineSeries currSeries = pointSeries.get(i);
			if(!currSeries.isVisible()) continue;
			int color = currSeries.getColor();
			rect.setColor(color);
			canvas.drawRect(left, top+(counter*size), right, bottom+(counter*size), rect);
			if((bottom-top)>24)			
				canvas.drawText(Integer.toString(counter+1), 0, 1, left+(right-left)/2, (top+(counter*size))+(bottom-top)/2+(textsize/2), text);
			
			counter++;		
		}			
	}
}