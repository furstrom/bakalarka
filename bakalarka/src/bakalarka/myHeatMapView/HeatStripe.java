package bakalarka.myHeatMapView;

import android.graphics.Canvas;
import android.graphics.Paint;
import bakalarka.helps.MyColors;
import bakalarka.myGraphView.DataPoint;
import bakalarka.myGraphView.GraphLineSeries;

//class representing one heat stripe in heatMap
//one stripe representing one wifi network
public class HeatStripe {

	private GraphLineSeries series;
	private ColorGradient heatMapGradient;
	private Canvas canvas;
	private int nmbrOfPoints;
	
	//series contains measured data from which we will make heat frames
	public HeatStripe(GraphLineSeries series){
		this.series=series;	
	}
	
	public void draw(int startX,float startY,Canvas canvas, MapParams mapParams){
		this.heatMapGradient = mapParams.getColorGradient(); //reference to gradient instance
		this.canvas=canvas; //reference to canvas
		final int lineWidth=(int)mapParams.getAxisLineWidth(); //width of drawed line 
		final int minFrameWidth=10; //minimal width of each frame
		final int nmbrOfSeries=mapParams.getNumberOfVisibleAp(); //how many networks are currently shown
		final int maxFrameCount = (int)Math.floor((float)(mapParams.getMapWidth()-lineWidth)/minFrameWidth); //how many frames fit into our heatMap
		mapParams.setMaxFrameCount(maxFrameCount);
		nmbrOfPoints = series.size(); //number points in series
		final float frameHeight = ((float)mapParams.getMaphHeight())/(float)nmbrOfSeries; //height of one frame contains in heatMap
		float frameWidth;
	
		float x = startX; //start position of first frame
		float y = startY; //start position of first frame
		
		Paint frame = new Paint();
		frame.setStyle(Paint.Style.FILL);
		
			
			// if there is less values then maximal frame count allowed:
			if (nmbrOfPoints <= maxFrameCount) {
				frameWidth = (float)mapParams.getMapWidth()/(float)nmbrOfPoints;

				for (int j = 0; j < nmbrOfPoints; j++) {
					DataPoint point = series.get(j);
					int value = point.getValue();
					drawFrame(x,y,x+frameWidth,y+frameHeight,value,frame);			
					x = x + frameWidth;
				}			
			}
			// if there is more values we have to merge:
			else {
				int[] values = mergePoints(maxFrameCount);
				frameWidth = Math.round((float)mapParams.getMapWidth()/maxFrameCount);
				
				for (int j = 0; j < maxFrameCount; j++) {
					int value = values[j];
					drawFrame(x,y,x + frameWidth,y + frameHeight,value,frame);
					x = x + frameWidth;
				}	
			}
			
			drawDescribe(startX, startY, frameHeight);
		
	}
	
	//draw info-text into stripe
	private void drawDescribe(float x,float y,float frameHeight){
		float textSize = 30;
		if(frameHeight <= textSize) return;
		Paint text = new Paint();
		text.setTextSize(textSize);
		StringBuilder describeText = new StringBuilder();
		describeText.append(series.getName());
		describeText.append(" ");
	    String stringText = series.getMac().substring(series.getMac().length()-5);	
		describeText.append(stringText);
		canvas.drawText(describeText.toString(), x+10, y+40, text);		
	}
	
	//drawing heat frame
	private void drawFrame(float left,float top,float right,float bottom,int value, Paint frame){
		value = Math.abs(value);
		float gradientValue = (value  - 0f) /(100 - 0f);
		float[] rgbCol = heatMapGradient.getColorAtValue(gradientValue);
		int color = MyColors.getIntFromColor(rgbCol[0], rgbCol[1], rgbCol[2]);
		frame.setColor(color);
		canvas.drawRect(left, top, right, bottom,frame);	
	}
	
	//merge data from GraphLineSeries to array maxFrameCount size
	private int[] mergePoints(int maxFrameCount) {
		int[] values = new int[maxFrameCount];
		int splitSize = (int) Math.floor((float) nmbrOfPoints / (float) maxFrameCount); //how many datas will merged to one
		int modulo = nmbrOfPoints %  maxFrameCount; //if splitSize is not whole number -> module!=0 
		int index = 0;
		for (int start = 0; start < nmbrOfPoints; start += splitSize) {
			int count = 0;
			for (int i = 0; i < splitSize; i++) {
				if ((start + i) >= nmbrOfPoints) {
					splitSize = i;
					break;
				}
				int value = series.get(start + i).getValue();
				value = Math.abs(value);
				count += value;			
				
			}			
			if(modulo!=0){ //till the module isnt zero, we are counting one more value to each sub-array 
				int value = series.get(start + splitSize).getValue();
				value = Math.abs(value);
				count += value;
				start++;
				modulo--;
				count = count / (splitSize+1);
			}else			
			count = count / splitSize;
			values[index] = count;
			index++;
		}
		return values;
	}	
	
	public boolean isVisible(){
		return this.series.isVisible();
	} 
	
}
