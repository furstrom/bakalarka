package bakalarka.GraphActivity.Fragments;

import java.util.ArrayList;
import java.util.List;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MyFragmentPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<Fragment>();
    private final List<String> mFragmentTitleList = new ArrayList<String>();

    public MyFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }


	public void addFragment(Fragment fragment,String title){
    	mFragmentList.add(fragment);
    	mFragmentTitleList.add(title);
    }
    
    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    @Override
    public Fragment getItem(int position) {
    	return mFragmentList.get(position);
      
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return mFragmentTitleList.get(position);
    }
}
