package bakalarka.GraphActivity.Fragments;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import bakalarka.myGraphView.DataPoint;
import bakalarka.myGraphView.GraphLineSeries;
import bakalarka.myGraphView.GraphScatterSeries;

public class GraphDataThreadScanner implements Runnable {
	
	private final WifiManager mWifiManager;
	private final HashMap<String, GraphLineSeries> mHashMapGraphLineSeries;
	private final HashMap<String, GraphScatterSeries> mHashMapGraphScatterSeries;
	private final boolean SCANNING = true;
	private List<ScanResult> scanResults;
	private long lastTime,currentTime;
	
	public GraphDataThreadScanner(WifiManager wifiManager,HashMap<String, GraphLineSeries> hashMapGraphLineSeries,
			HashMap<String, GraphScatterSeries> hashMapGraphScatterSeries) {
		mWifiManager = wifiManager;
		mHashMapGraphLineSeries = hashMapGraphLineSeries;
		mHashMapGraphScatterSeries = hashMapGraphScatterSeries;
		lastTime = System.currentTimeMillis();
		currentTime = System.currentTimeMillis();
	}

	@Override
	public void run() {
		while(SCANNING){
			currentTime = System.currentTimeMillis(); 
			if((currentTime-lastTime)>=50){
				doScan();
				lastTime = currentTime;
			}			
		}
	}
	
	@SuppressWarnings("rawtypes")
	private void doScan(){
		mWifiManager.startScan();
		scanResults = mWifiManager.getScanResults();
		GraphLineSeries currentLineSeries;
		GraphScatterSeries currentScatterSeries;
		
		Iterator it = mHashMapGraphLineSeries.entrySet().iterator();
		// iterate trough all GraphValues in HashMap
		while (it.hasNext()) {
			boolean isContain = false;
			Map.Entry pair = (Map.Entry) it.next();
			String key = (String) pair.getKey();
			currentLineSeries = mHashMapGraphLineSeries.get(key);
			currentScatterSeries = mHashMapGraphScatterSeries.get(key);
			// go trough all scanResults
			for (int i = 0; i < scanResults.size(); i++) {
				// if actual APs dataPontList is in new sncanResults
				// then add a new value
				if (key.equals(scanResults.get(i).BSSID)) {
					DataPoint newDataPoint = new DataPoint(scanResults.get(i).level);
					currentLineSeries.add(newDataPoint);
					currentScatterSeries.add(newDataPoint);
					isContain = true;
					break;
				}
			}
			// if actual APs dataPontList is NOT in a new scanResult
			// then set a new value to -100
			if (!isContain) {
				DataPoint newDataPoint = new DataPoint(-99);
				currentLineSeries.add(newDataPoint);
				currentScatterSeries.add(newDataPoint);
			}			
		}
	}
}
