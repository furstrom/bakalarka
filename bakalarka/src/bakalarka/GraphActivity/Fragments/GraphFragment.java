package bakalarka.GraphActivity.Fragments;

import java.util.ArrayList;
import java.util.HashMap;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import bakalarka.helps.MyColors;
import bakalarka.helps.RotationChangeHandler;
import bakalarka.myGraphView.GraphLineSeries;
import bakalarka.myGraphView.GraphScatterSeries;
import bakalarka.myGraphView.GraphView;
import bakalarka.myGraphView.graphLegend.MyGraphItemListAdapter;
import bakalarka.wifiNetworksList.AccessPoint;

import com.example.bakalarka.R;



public class GraphFragment extends Fragment implements OnItemClickListener{
	
	// Activity must implement this interface
    public interface OnGraphItemSelectedListener {
        public void onGraphItemSelected(int position);
    }

	private OnGraphItemSelectedListener mCallBack;
	private GraphView mGraphView; //graphView
	private ListView mLegendListView; //legends
	//private LinearLayout mCheckLayout; //checkable box about showing tolerance in scatters
	private ArrayList<AccessPoint> accessPoints; //list of APs
	/* 
	 * hash map strength values for given networks
	 * Key - String - MAC address of AP
	 * Value - LineGraphSeries<DataPoint> - graph line series of AP 
	*/
	private HashMap<String, GraphLineSeries> mHashMapGraphLineSeries;
	private HashMap<String, GraphScatterSeries> mHashMapGraphScatterSeries;
	private LegendContent mLegendContent;//content of legend for graph 
	private MyGraphItemListAdapter mAdapter; //adapter for listView

     public GraphFragment() {
    	 mHashMapGraphLineSeries = new HashMap<String,GraphLineSeries>();        
         mHashMapGraphScatterSeries = new HashMap<String,GraphScatterSeries>();
	}
 
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.graphfragment, container, false);
        return rootView;
    }
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
        mGraphView = (GraphView)view.findViewById(R.id.graphView);
        mLegendListView = (ListView)view.findViewById(R.id.graphLegendView);	
        //mCheckLayout = (LinearLayout)view.findViewById(R.id.checkable_layout);
        makeLegend();     
        initGraph();
        
        int orientation = getActivity().getResources().getConfiguration().orientation;
        getActivity().getResources().getConfiguration();
		int  landscape = Configuration.ORIENTATION_LANDSCAPE;
		 if(orientation==landscape ){
			 RotationChangeHandler.graph(mGraphView,mLegendListView);
		 }
	}

	
	@Override
	public void onAttach(Context context) {
		 super.onAttach(context);	 
		 mCallBack = (OnGraphItemSelectedListener)getActivity();
		
	}
	
	//when screen orientation is changed	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
			
			RotationChangeHandler.graph(newConfig,mGraphView,mLegendListView);
			mGraphView.setShowScatters(false);
		}

	/*
	 * inicialization of hash map KEY - String - bssid of AP
	 * 							VALUE - LineGraphSeries<DataPoint - empty GraphSeries 
	 */
	private void initHashMap() {

		for (int i = 0; i < accessPoints.size(); i++) {
			mHashMapGraphLineSeries.put(accessPoints.get(i).getMacAddress(),
					new GraphLineSeries());	
			mHashMapGraphScatterSeries.put(accessPoints.get(i).getMacAddress(),
					new GraphScatterSeries());
		}
	}

	// make a graph legend
	private void makeLegend() {
		mLegendContent = new LegendContent(accessPoints);	
		mAdapter = new MyGraphItemListAdapter(getContext(),mLegendContent.getItems());
		mLegendListView.setAdapter(mAdapter);
		mLegendListView.setOnItemClickListener(this);
		mLegendContent.setAllItemChecked(mLegendListView);

		
	}

	// initialization of empty graph
	private void initGraph() {
		for (int i = 0; i < accessPoints.size(); i++) {
			AccessPoint actualAP = accessPoints.get(i);
			int freq = actualAP.getFrequency();
			String secur = actualAP.getSecure();
			String mac = actualAP.getMacAddress();
			String name = actualAP.getNetwork();
			int color = MyColors.get(i);

			GraphLineSeries lineSeries = mHashMapGraphLineSeries.get(mac);
			lineSeries.setName(name);
			lineSeries.setFrequency(freq);
			lineSeries.setSecure(secur);
			lineSeries.setMac(mac);
			lineSeries.setColor(color);
			mGraphView.addLineSeries(lineSeries);
			GraphScatterSeries scatterSeries = mHashMapGraphScatterSeries
					.get(mac);
			scatterSeries.setName(name);
			scatterSeries.setColor(color);
			scatterSeries.setNumberOfScatters(5);
			mGraphView.addScatterSeries(scatterSeries);
		}
	}
	
	public GraphView getGraph(){
		synchronized (this) {
			return this.mGraphView;
		}
	}
	public ListView getLegend(){
		return this.mLegendListView;
	}
		
	public void setLegendContent(LegendContent mLegendContent) {
		this.mLegendContent = mLegendContent;
	}

	public HashMap<String, GraphLineSeries> getHashMapGraphLineSeries() {
		return mHashMapGraphLineSeries;
	}

	public HashMap<String, GraphScatterSeries> getHashMapGraphScatterSeries() {
		return mHashMapGraphScatterSeries;
	}

	public void setAccessPoints(ArrayList<AccessPoint> accessPoints) {
		this.accessPoints = accessPoints;
		initHashMap();
	}
	
	public void switchScatterSetting(){
		mGraphView.switchScatterSetting();
		
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		mCallBack.onGraphItemSelected(position);
	}
	
	public void clickListViewItem(int position){
		boolean value = mLegendContent.isItemSelected(position);;
		value = !value;
		mLegendContent.setItemChecked(position, value);//have to update number of currently shown APs
		mGraphView.setVisibleSeries(position,value);
		mLegendListView.setItemChecked(position, value);
		
	}
	
	@Override
	public void onPause() {
		super.onPause();
		//mGraphView.setDrawing(false);
	}	
	@Override
	public void onResume() {
		super.onResume();
		//mGraphView.setDrawing(true);
	}
	@Override
	public void onStop() {
		super.onResume();
		//mGraphView.setDrawing(false);
	}

}
