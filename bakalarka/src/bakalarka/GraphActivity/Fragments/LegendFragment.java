package bakalarka.GraphActivity.Fragments;

import java.util.ArrayList;

import bakalarka.myGraphView.graphLegend.MyGraphItemListAdapter;
import bakalarka.wifiNetworksList.AccessPoint;

import com.example.bakalarka.R;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class LegendFragment  extends Fragment implements OnItemClickListener{
	
	// Activity must implement this interface
    public interface OnLegendItemSelectedListener {
        public void onLegendItemSelected(int position);
    }
    
    private OnLegendItemSelectedListener mCallBack;
    private ListView mLegendListView; //legend
    private ArrayList<AccessPoint> accessPoints; //list of APs
    private LegendContent mLegendContent;//content of legend for graph 
	private MyGraphItemListAdapter mAdapter; //adapter for listView

	public LegendFragment(){	
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.legendfragment, container, false);
        return rootView;
    }
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mLegendListView = (ListView)view.findViewById(R.id.legendListView);
		makeLegend();
	}
	
	@Override
	public void onAttach(Context context) {
		 super.onAttach(context);	 
		 mCallBack = (OnLegendItemSelectedListener)getActivity();
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		mCallBack.onLegendItemSelected(position);
		
	}
	
	public void clickListViewItem(int position){
		boolean value = mLegendContent.isItemSelected(position);;
		value = !value;
		mLegendContent.setItemChecked(position, value);//have to update number of currently shown APs
		mLegendListView.setItemChecked(position, value);
		
	}
	
	// make a graph legend
	private void makeLegend() {
		mLegendContent = new LegendContent(accessPoints);
		mAdapter = new MyGraphItemListAdapter(getContext(),mLegendContent.getItems());
		mLegendListView.setAdapter(mAdapter);
		mLegendListView.setOnItemClickListener(this);
		mLegendContent.setAllItemChecked(mLegendListView);

	}

	public void setAccessPoints(ArrayList<AccessPoint> accessPoints) {
		this.accessPoints = accessPoints;
	}
	
	public void setLegendContent(LegendContent mLegendContent) {
		this.mLegendContent = mLegendContent;
	}
}
