package bakalarka.GraphActivity.Fragments;

import bakalarka.helps.Convert;
import bakalarka.myGraphView.GraphView;
import bakalarka.myHeatMapView.HeatMapView;
import android.os.Handler;

public class GraphHandlerDataRender {
	private Handler mHandler;
	private GraphView mGraph;
	private HeatMapView mHeatMap;
	private int MEASURES_IN_SEC = 20; //how many measures in 1sec (20x50ms)
	
	private GraphFragment graphFragment;
	private HeatmapFragment heatMapFragment;

	
	public GraphHandlerDataRender(GraphView graph, HeatMapView heatMap){
		mGraph = graph;
		mHandler = new Handler();
		mHeatMap = heatMap;
	}
	
	public GraphHandlerDataRender(GraphFragment graphFragment, HeatmapFragment heatMapFragment){
		mHandler = new Handler();
		this.graphFragment = graphFragment;
		this.heatMapFragment = heatMapFragment;
	
	}
		
	public void start(){
		mHandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				
				//if(mGraph==null || mHeatMap==null){
					mGraph=graphFragment.getGraph();
					mHeatMap=heatMapFragment.getHeatmap();
					//if(mGraph!=null && mHeatMap!=null)mHeatMap.setPointSeries(mGraph.getGraphLinePointSeries());					
				//}else{
					mHeatMap.setPointSeries(mGraph.getGraphLinePointSeries());
				int endPoint=mGraph.getEndLineFrame()+1; //last point shown in graph line
				int startPoint = mGraph.getStartLineFrame()+1; //first point shown in graph line
				int linePointsNumber = mGraph.getLinePointsNumber();
				int dataNumber = mGraph.getNumberOfMeasuredData();
				
				mGraph.redrawLineSeries();
				
				//every 20 measures (=1sec) we want to redraw/update X scale value
				if(endPoint%MEASURES_IN_SEC==0){
					String value = Convert.getTimeFormat(endPoint/20);
					mGraph.setScaleX(4, value);
					if(startPoint>=20){
						value = Convert.getTimeFormat(startPoint/20);
						mGraph.setScaleX(3, value);						
					}else mGraph.setScaleX(3, "");					
					mGraph.redrawXScale();
				}
				//if data line touch the border - update X scale value
				if(endPoint >=(linePointsNumber+MEASURES_IN_SEC) && endPoint%MEASURES_IN_SEC==0){
					String value = Convert.getTimeFormat((double)(endPoint-linePointsNumber)/40);
					mGraph.setScaleX(2, value);
					mGraph.redrawXScale();
					mGraph.redrawScatterSeries();
				//if data line not touch border - hide scatters 
				}else if(endPoint < (linePointsNumber+20)){ 
					mGraph.setShowScatters(false);
					mGraph.setScaleX(2, "");
				}	
				//redrawing heatmap
				if(mHeatMap.getMaxFrameCount()!=0 && (dataNumber%mHeatMap.getMaxFrameCount())==0) 
					mHeatMap.redraw();
				//}
				start();						
			}
		}, 50);		
	}

}
