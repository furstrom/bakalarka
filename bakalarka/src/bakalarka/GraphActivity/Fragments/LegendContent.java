package bakalarka.GraphActivity.Fragments;

import java.util.ArrayList;
import java.util.Arrays;

import android.widget.ListView;
import bakalarka.helps.MyColors;
import bakalarka.myGraphView.graphLegend.GraphLegendItem;
import bakalarka.myGraphView.graphLegend.IGraphLegendItem;
import bakalarka.wifiNetworksList.AccessPoint;

public class LegendContent {
	
	ArrayList<IGraphLegendItem> itemsList;
	/* every index 'i' is TRUE/FALSE depending on item 'i' in listView is selected or not */
	private boolean selectedItems[];
	private int numberOfSelected;
	
	public LegendContent(ArrayList<AccessPoint> accessPoints) {
		itemsList= new ArrayList<IGraphLegendItem>();
		
		for(int i=0;i<accessPoints.size();i++){
			AccessPoint ap = accessPoints.get(i);
			GraphLegendItem item = new GraphLegendItem(i+1,ap.getNetwork(), ap.getMacAddress(), ap.getFrequency(), ap.getSecure(), MyColors.get(i));
			itemsList.add(item);
			
		}		
		selectedItems = new boolean[accessPoints.size()];
		Arrays.fill(selectedItems, 	Boolean.TRUE);
	}
	
	public ArrayList<IGraphLegendItem> getItems(){
		return itemsList;
	}
	
	public boolean isItemSelected(int index){
		return selectedItems[index];
	}

	public void setItemChecked(int index,boolean value){
		selectedItems[index]=value;
		if(value==true) numberOfSelected++;
		else numberOfSelected--;
		
	}
	
	//set all items checked
	//param ListView
	public void setAllItemChecked(ListView listView){
		for(int i=0;i< itemsList.size();i++){		
			listView.setItemChecked(i, true);
			numberOfSelected++;
		}	
		Arrays.fill(selectedItems , true);
		
	}

	
	public int getNumberOfSelectedItems(){
		return this.numberOfSelected;
	}
}
