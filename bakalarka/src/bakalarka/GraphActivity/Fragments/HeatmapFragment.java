package bakalarka.GraphActivity.Fragments;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import bakalarka.helps.RotationChangeHandler;
import bakalarka.myGraphView.graphLegend.MyGraphItemListAdapter;
import bakalarka.myHeatMapView.HeatMapView;
import bakalarka.wifiNetworksList.AccessPoint;

import com.example.bakalarka.R;

public class HeatmapFragment extends Fragment  implements OnItemClickListener{
	
	// Activity must implement this interface
    public interface OnHeatmapItemSelectedListener {
        public void onHeatmapItemSelected(int position);
    }
	
    private OnHeatmapItemSelectedListener mCallBack;
	private HeatMapView mHeatmapView; //heatmap
	private ListView mLegendListView; //legends
	private ArrayList<AccessPoint> accessPoints; //list of APs
	private LegendContent mLegendContent;//content of legend for heatmap
	private MyGraphItemListAdapter mAdapter; //adapter for listView
	
	public HeatmapFragment(){
		
	}

	// make a graph legend
	private void makeLegend() {
		mLegendContent = new LegendContent(accessPoints);
		mAdapter = new MyGraphItemListAdapter(getContext(),mLegendContent.getItems());
		mLegendListView.setAdapter(mAdapter);
		mLegendListView.setOnItemClickListener(this);
		mLegendContent.setAllItemChecked(mLegendListView);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.heatmapfragment, container, false);
        return rootView;
    }
	
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
        mHeatmapView = (HeatMapView)view.findViewById(R.id.heatmapView);
        mLegendListView = (ListView)view.findViewById(R.id.heatmapLegendView);	
        makeLegend(); 
        mHeatmapView.setNmbrVisibleAp(mLegendContent.getNumberOfSelectedItems());
        
        int orientation = getActivity().getResources().getConfiguration().orientation;
        getActivity().getResources().getConfiguration();
		int  landscape = Configuration.ORIENTATION_LANDSCAPE;
		 if(orientation==landscape ){
			 RotationChangeHandler.heatMap(mHeatmapView,mLegendListView);
		 }
	}
	
	@Override
	public void onAttach(Context context) {
		 super.onAttach(context);	 
		 mCallBack = (OnHeatmapItemSelectedListener)getActivity();
	}
	
	// when screen orientation is changed
	@Override
	public void onConfigurationChanged(Configuration newConfig) {

		RotationChangeHandler.heatMap(newConfig, mHeatmapView, mLegendListView);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		mCallBack.onHeatmapItemSelected(position);
	}
	
	public void clickListViewItem(int position){
		boolean value = mLegendContent.isItemSelected(position);;
		value = !value;
		mLegendContent.setItemChecked(position, value);//have to update number of currently shown APs
		mHeatmapView.setNmbrVisibleAp(mLegendContent.getNumberOfSelectedItems());
		mLegendListView.setItemChecked(position, value);	
		//mHeatmapView.setNmbrVisibleAp(mLegendContent.getNumberOfSelectedItems());
	}
	
	public HeatMapView getHeatmap(){
		synchronized (this) {
			return mHeatmapView;
		}
	}
	public void setAccessPoints(ArrayList<AccessPoint> accessPoints) {
		synchronized (this) {
			this.accessPoints = accessPoints;
			}
	}
	public void setLegendContent(LegendContent mLegendContent) {
		synchronized (this) {
			this.mLegendContent = mLegendContent;
			}
	}

}
