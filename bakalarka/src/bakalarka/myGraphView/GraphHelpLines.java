package bakalarka.myGraphView;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

class GraphHelpLines {
	
	Canvas canvas;
	
	Point leftUp;
	Point leftDown;
	Point rightUp;
	int graphHeight;
	int borderXPosition;
	
	Point helpLine40Start;
	Point helpLine40End;
	Point helpLine50Start;
	Point helpLine50End;
	Point helpLine60Start;
	Point helpLine60End;
	Point helpLine70Start;
	Point helpLine70End;
	Point helpLine80Start;
	Point helpLine80End;
	Point helpLine90Start;
	Point helpLine90End;
	
	public GraphHelpLines(Canvas canvas,GraphParams params){
		this.canvas=canvas;
		
		leftUp = params.getLeftUpCorner();
		leftDown = params.getLeftDownCorner();
		rightUp = params.getRightUpCorner();
		graphHeight = params.getGraphHeight();
		borderXPosition=params.getBorderXPosition();
				
		helpLine40Start = new Point(leftUp.x,leftUp.y);
		helpLine40End = new Point(rightUp.x,rightUp.y);
		helpLine50Start = new Point(leftUp.x,leftUp.y+graphHeight/6);
		helpLine50End = new Point(rightUp.x,rightUp.y+graphHeight/6);
		helpLine60Start = new Point(leftUp.x,leftUp.y+graphHeight*2/6);
		helpLine60End = new Point(rightUp.x,rightUp.y+graphHeight*2/6);
		helpLine70Start = new Point(leftUp.x,leftUp.y+graphHeight*3/6);
		helpLine70End = new Point(rightUp.x,rightUp.y+graphHeight*3/6);
		helpLine80Start = new Point(leftUp.x,leftUp.y+graphHeight*4/6);
		helpLine80End = new Point(rightUp.x,rightUp.y+graphHeight*4/6);
		helpLine90Start = new Point(leftUp.x,leftUp.y+graphHeight*5/6);
		helpLine90End = new Point(rightUp.x,rightUp.y+graphHeight*5/6);		
	}
	
	public void draw(){
		Paint helpLine = new Paint();
		helpLine.setColor(Color.parseColor("#363636"));
		helpLine.setStrokeWidth(1);
		canvas.drawLine(helpLine40Start.x, helpLine40Start.y, helpLine40End.x, helpLine40End.y, helpLine);
		canvas.drawLine(helpLine50Start.x,helpLine50Start.y,helpLine50End.x,helpLine50End.y ,helpLine);
		canvas.drawLine(helpLine60Start.x,helpLine60Start.y,helpLine60End.x,helpLine60End.y ,helpLine);;
		canvas.drawLine(helpLine70Start.x,helpLine70Start.y,helpLine70End.x,helpLine70End.y ,helpLine);
		canvas.drawLine(helpLine80Start.x,helpLine80Start.y,helpLine80End.x,helpLine80End.y ,helpLine);
		canvas.drawLine(helpLine90Start.x,helpLine90Start.y,helpLine90End.x,helpLine90End.y ,helpLine);
		
		Paint border= new Paint();
		border.setColor(Color.GRAY);
		border.setStrokeWidth(2);
		canvas.drawLine(borderXPosition, leftDown.y, borderXPosition, leftUp.y, border);		
	}

}
