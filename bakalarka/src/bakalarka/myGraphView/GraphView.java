package bakalarka.myGraphView;

import java.util.ArrayList;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class GraphView extends View{
	
	Bitmap b; 
	Point size = new Point();
	List<GraphLineSeries> graphSeries;
	List<GraphScatterSeries> graphScatters;
	private boolean drawing = true;
	private GraphParams graphParams=null;
	private GraphAxis graphAxis;
	private GraphHelpLines graphHelpLines;
	private GraphScaleY graphScaleY;
	private GraphScaleX graphScaleX=new GraphScaleX();
	private GraphLineContent graphLineContent;
	private GraphScattersContent graphScattersContent;
	private boolean showScatter=false;
	
	public GraphView(Context context) {
		super(context);
		init();
	}
	
	public GraphView(Context context, AttributeSet attrs){
		super(context,attrs);
		init();
	}
	
	private void init(){
		b = Bitmap.createBitmap(100,100,Bitmap.Config.ARGB_8888);
		graphSeries = new ArrayList<GraphLineSeries>();
		graphScatters = new ArrayList<GraphScatterSeries>(); 			
		graphParams = new GraphParams();
		graphLineContent = new GraphLineContent(graphSeries);
		graphScattersContent = new GraphScattersContent(graphScatters);	
	}
	
	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas){
		super.onDraw(canvas);
		if (drawing == true) {
			drawGraphSheet(canvas);
			if (graphSeries != null || !graphSeries.isEmpty()) {
				drawSeriesContent(canvas);
			}
			if (showScatter == true) {
				drawScattersContent(canvas);
			}
		}	
	}
	
	//draw graphSheet to the canvas
	private void drawGraphSheet(Canvas canvas){
		graphParams.set(canvas);
		graphAxis=new GraphAxis(canvas, graphParams);
		graphHelpLines = new GraphHelpLines(canvas, graphParams);
		graphScaleY = new GraphScaleY(canvas, graphParams);	
		graphAxis.draw();
		graphHelpLines.draw();		
		graphScaleY.draw();
		graphScaleX.draw(canvas,graphParams);
	}
	
	//handle screen touch
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent ev){
		
		GraphToucheventHandler.handleTouchEvent(ev,graphParams);
		if(ev.getPointerCount()>1) updateScatters();
		this.invalidate();
		return true;		
	}
		
	//draw lineSeries in graph
	public void drawSeriesContent(Canvas canvas){
		graphLineContent.draw(canvas, graphParams);		
		if(graphSeries.isEmpty() ) return;
	}
	
	//draw scatters in graph
	private void drawScattersContent(Canvas canvas) {
		graphScattersContent.draw(canvas, graphParams);
	}
	
	//recalculate the scatters values
	public void updateScatters(){
		int ScattersNumber = graphParams.getScattersNumber();
		graphScattersContent.updateScatters(ScattersNumber);
	}
	
	//change type of scatters 
	public void switchScatterSetting(){
		graphParams.switchScatterMinMaxShowing();
		redrawScatterSeries();
		
		
	}
		
	//redraw area where  the lineSeries are
	public void redrawLineSeries(){
		int left=graphParams.getBorderXPosition();
		int top=graphParams.getLeftUpCorner().y;
		int right=graphParams.getRightDownCorner().x;
		int bottom=graphParams.getRightDownCorner().y;
		this.invalidate(left, top, right, bottom);
	}
	
	//redraw area where the scatters are
	public void redrawScatterSeries(){
		showScatter=true;
		updateScatters();
		int left=graphParams.getLeftUpCorner().x;
		int top=graphParams.getLeftUpCorner().y;
		int right=graphParams.getBorderXPosition();
		int bottom=graphParams.getRightDownCorner().y;
		this.invalidate(left, top, right, bottom);		
	}
	
	//redraw X scale
	public void redrawXScale(){
		int left=0;
		int top=graphParams.getLeftDownCorner().y-1;
		int right=graphParams.getCanvasWidth();
		int bottom=graphParams.getCanvasHeight();;
		this.invalidate(left, top, right, bottom);
	}
	
	//setting values on X axe
	public void setScaleX(int i,String value){
		graphScaleX.setValue(i, value);
	}

	//add line series to the graph
	public void addLineSeries(GraphLineSeries series){
		graphSeries.add(series);	
	}
	
	//add acatters to the graph
	public void addScatterSeries(GraphScatterSeries series){
		graphScatters.add(series);
	}
	
	//return number of point in LinePointArea
	public int getLinePointsNumber(){
		synchronized (this) {
			return graphParams.getLinePointsNumber();
		}
	}
	//return size value between each point in LinePointArea
	public void setSizeBetweenLinePonts(int i){
		graphParams.setSizeBetweenLinePoints(i);
	}
	//do we want to show the scatters in graph ?
	public void setShowScatters(boolean bool){
		this.showScatter=bool;
	}
	//setting scrolling value during multiTouch event
	public void setScrollLineSeriesIndex(int move){
		graphParams.setScrollLineSeriesIndex(move);
	}
	//return list of data for lineSeries
	public List<GraphLineSeries> getGraphLinePointSeries(){
		return graphSeries;		
	}
	
	//return number of all data measured so far
	public int getNumberOfMeasuredData(){
		if(!graphSeries.isEmpty())
			return graphSeries.get(0).size();
		else return 0;
	}

	//return number of the point where line frame begin
	public int getStartLineFrame() {
		return graphParams.getStartLineFrame();
	}


	public int getEndLineFrame() {
		return graphParams.getEndLineFrame();
	}
	
	//set drawing on canvas
	public void setDrawing(boolean value){
		this.drawing = value;
	}
	
	//set visible series in graph
	public void setVisibleSeries(int position,boolean val){
		graphSeries.get(position).setVisible(val);
		graphScatters.get(position).setVisible(val);
	}
}
