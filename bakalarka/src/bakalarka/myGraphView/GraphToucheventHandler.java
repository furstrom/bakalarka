package bakalarka.myGraphView;

import bakalarka.helps.MyMath;
import android.view.MotionEvent;

public final class GraphToucheventHandler {
	
	private static final int INVALID_POINTER_ID = -1;
	private static final int REDUCTION_VALUE = 4;
	private static int firstActivePointerId = INVALID_POINTER_ID;
	private static int secondActivePointerId = INVALID_POINTER_ID;
	private static float mPrevFirstFingerX,mPrevFirstFingerY;
	private static float mPrevSecondFingerX,mPrevSecondFingerY;
	private static GraphParams graphParams;
	
	public static void handleTouchEvent(MotionEvent ev, GraphParams params){
		final int action = ev.getAction();
		graphParams = params;
		
		switch (action & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_DOWN: {
			final float x = ev.getX();
			final float y = ev.getY();
			mPrevFirstFingerX = x;
			mPrevFirstFingerY = y;
						
			// save ID of first point
			firstActivePointerId = ev.getPointerId(0);			
			break;			
		}		
		case MotionEvent.ACTION_POINTER_DOWN:{
			if(secondActivePointerId == INVALID_POINTER_ID){
				// extract the index of pointer that touch the screen
				final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
				final float x = ev.getX(pointerIndex);
				final float y = ev.getY(pointerIndex);
							
				mPrevSecondFingerX = x;
				mPrevSecondFingerY = y;
				secondActivePointerId = ev.getPointerId(pointerIndex);			
			}
			break;			
		}
		
		case MotionEvent.ACTION_UP:{
			firstActivePointerId = INVALID_POINTER_ID;
			secondActivePointerId=INVALID_POINTER_ID;
			break;
		}
		
		case MotionEvent.ACTION_POINTER_UP:{
			final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
			if(pointerIndex==secondActivePointerId){
				secondActivePointerId=INVALID_POINTER_ID;
				mPrevSecondFingerX=0;mPrevSecondFingerY=0;				
				break;
			}
			if(pointerIndex==firstActivePointerId){
				firstActivePointerId = secondActivePointerId;
				secondActivePointerId=INVALID_POINTER_ID;
				mPrevFirstFingerX=mPrevSecondFingerX;
				mPrevFirstFingerY=mPrevSecondFingerY;
				mPrevSecondFingerX=0;mPrevSecondFingerY=0;
			}
			break;
			
		}
		
		case MotionEvent.ACTION_CANCEL:{
			firstActivePointerId = INVALID_POINTER_ID;
			secondActivePointerId = INVALID_POINTER_ID;
			break;
		}
		case MotionEvent.ACTION_MOVE:{	
			int touchedPoints = ev.getPointerCount();
			if (touchedPoints == 1) {
			
				float currX = ev.getX(0);
				float currY = ev.getY(0);
				int size = MyMath.euclidSize(currX, currY, mPrevFirstFingerX, mPrevFirstFingerY);
				if(currX<mPrevFirstFingerX){
					graphParams.setScrollLineSeriesIndex(size/REDUCTION_VALUE);
					
				}else if(currX>mPrevFirstFingerX){
					graphParams.setScrollLineSeriesIndex(-size/REDUCTION_VALUE);
				}
				mPrevFirstFingerX=currX;
			} else {
				float currX1 = mPrevFirstFingerX; 
				float currX2 = mPrevSecondFingerX;
				float currY1 = mPrevFirstFingerY; 
				float currY2 = mPrevSecondFingerY;
				for (int index = 0; index < touchedPoints; index++) {

					int id = ev.getPointerId(index);
					if (id == firstActivePointerId) {
						currX1 = ev.getX(index);
						currY1 = ev.getY(index);
					}
					if (id == secondActivePointerId) {
						currX2 = ev.getX(index);
						currY2 = ev.getY(index);
					}
				}
				int prevEuclid = MyMath.euclidSize(mPrevFirstFingerX, mPrevFirstFingerY, 
						mPrevSecondFingerX, mPrevSecondFingerY);
				int currEuclid = MyMath.euclidSize(currX1, currY1, currX2, currY2);
				int diff = Math.abs(prevEuclid-currEuclid)/REDUCTION_VALUE;
				
				if (prevEuclid>currEuclid) {

					graphParams.moveBorder(diff);
				}
				else{
					graphParams.moveBorder(-diff);
				}
				mPrevFirstFingerX = currX1;
				mPrevSecondFingerX = currX2;
				mPrevFirstFingerY = currY1;
				mPrevSecondFingerY = currY2;
			}
		}
		}
	}
}
