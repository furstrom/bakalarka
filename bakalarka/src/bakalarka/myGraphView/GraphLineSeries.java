package bakalarka.myGraphView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;

public class GraphLineSeries implements List<DataPoint>,Parcelable{
	
	private String name;
	private String mac;
	private String secure;
	private int frequency;
	private int color;
	private ArrayList<DataPoint> dataList;
	private boolean visible;
	
	public GraphLineSeries(){
		long time = System.currentTimeMillis();
		name = "GraphLineSeries" + Long.toString(time);
		color = Color.WHITE;
		dataList = new ArrayList<DataPoint>();
		visible = true;
	}
	
	public GraphLineSeries(String name, int color) {
		this.name = name;
		this.color = color;
		this.dataList = new ArrayList<DataPoint>();
		visible = true;
	}
	
	/*
	 * Constructor to use when re-constructing object
	 * from a parcel
	 * 
	 * @param in a parcel from which to read this object
	 * */
	public GraphLineSeries(Parcel in){
		this.name = in.readString();
		this.mac = in.readString();
		this.secure = in.readString();
		this.frequency = in.readInt();
		this.dataList = new ArrayList<DataPoint>();
		in.readTypedList(dataList, DataPoint.CREATOR);
			
	}
	
	public boolean isVisible(){
		return this.visible;
	}
	
	public void setVisible(boolean val){
		this.visible = val;
	}
	
	public void setMac(String mac) {
		this.mac = mac;
	}

	public void setSecure(String secure) {
		this.secure = secure;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public void setColor(int color){
		this.color = color;
	}
	
	public int getColor(){
		return color;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public String getMac() {
		return mac;
	}

	public String getSecure() {
		return secure;
	}

	public int getFrequency() {
		return frequency;
	}

	@Override
	public boolean add(DataPoint dataPoint) {
		synchronized (this) {
			return dataList.add(dataPoint);
		}
	}

	@Override
	public void add(int index, DataPoint dataPoint) {
		synchronized (this) {
			dataList.add(index, dataPoint);
		}
		
	}

	@Override
	public boolean addAll(Collection<? extends DataPoint> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(int arg0, Collection<? extends DataPoint> arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean contains(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public DataPoint get(int index) {
		synchronized (this) {
			return dataList.get(index);
		}
	}

	@Override
	public int indexOf(Object dataPoint) {
		synchronized (this) {
			return dataList.indexOf(dataPoint);
		}
	}

	@Override
	public boolean isEmpty() {
		synchronized (this) {
			return dataList.isEmpty();
		}
	}

	@Override
	public Iterator<DataPoint> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int lastIndexOf(Object arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ListIterator<DataPoint> listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator<DataPoint> listIterator(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DataPoint remove(int index) {
		return dataList.remove(index);
	}

	@Override
	public boolean remove(Object dataPoint) {
		return dataList.remove(dataPoint);
	}

	@Override
	public boolean removeAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public DataPoint set(int index, DataPoint dataPoint) {
		return dataList.set(index, dataPoint);
	}

	@Override
	public int size() {
		synchronized (this) {
			return dataList.size();
		}	
	}

	@Override
	public List<DataPoint> subList(int start, int end) {
		synchronized (this) {
			return dataList.subList(start, end);
		}
	}

	@Override
	public Object[] toArray() {
		return dataList.toArray();
	}

	@Override
	public <T> T[] toArray(T[] dataArray) {
		return dataList.toArray(dataArray);
	}

	/*
	 * This field is needed for Android to be able to
	 * create new objects, individually or as arrays 
	 */
	public static final Parcelable.Creator<GraphLineSeries> CREATOR = 
			new Parcelable.Creator<GraphLineSeries>() {
				@Override
				public GraphLineSeries createFromParcel(Parcel in) {
					return new GraphLineSeries(in);
				}	
				@Override
				public GraphLineSeries[] newArray(int size) {
					return new GraphLineSeries[size];
				}
	};

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(name);
		dest.writeString(mac);
		dest.writeString(secure);
		dest.writeInt(frequency);
		dest.writeTypedList(dataList);
		
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

}
