package bakalarka.myGraphView;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

 class GraphAxis {
	private Canvas canvas;
	private Point leftUp;
	private Point leftDown;
	private Point rightDown ;
	private float scatterAreaSize;
	private float strokeWidth;
	private int color;
	
	public GraphAxis(Canvas canvas, GraphParams graphParams){
		this.canvas=canvas;
		
		//set to default values
		leftUp = graphParams.getLeftUpCorner();
		leftDown = graphParams.getLeftDownCorner();
		rightDown = graphParams.getRightDownCorner();
		scatterAreaSize = graphParams.getScatterAreaSize();
		strokeWidth = 3;
		color = Color.WHITE;			
	}
	
	public void draw(){
		Paint axis = new Paint();
		axis.setColor(Color.WHITE);
		axis.setStrokeWidth(strokeWidth);
		canvas.drawLine(leftUp.x, leftUp.y, leftDown.x, leftDown.y, axis);
		canvas.drawLine(leftDown.x, leftDown.y, rightDown.x, rightDown.y, axis);
		canvas.drawLine(leftDown.x+scatterAreaSize/2, leftDown.y, leftDown.x+scatterAreaSize/2, leftDown.y+8, axis);		
	}


	public Point getLeftUp() {
		return leftUp;
	}


	public void setLeftUp(Point leftUp) {
		this.leftUp = leftUp;
	}

	public Point getLeftDown() {
		return leftDown;
	}


	public void setLeftDown(Point leftDown) {
		this.leftDown = leftDown;
	}

	public Point getRightDown() {
		return rightDown;
	}


	public void setRightDown(Point rightDown) {
		this.rightDown = rightDown;
	}


	public float getScatterAreaSize() {
		return scatterAreaSize;
	}

	public void setScatterAreaSize(float scatterAreaSize) {
		this.scatterAreaSize = scatterAreaSize;
	}

	public float getStrokeWidth() {
		return strokeWidth;
	}

	public void setStrokeWidth(float strokeWidth) {
		this.strokeWidth = strokeWidth;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}
	
	
}
