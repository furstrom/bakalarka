package bakalarka.myGraphView;

import java.util.List;

import android.graphics.Canvas;
import android.graphics.Paint;

//representing content of our graph
//drawing line series 
public class GraphLineContent {
	private List<GraphLineSeries> pointSeries;
	private GraphParams graphParams;
	// how many points in lineArea
	private int linePointsNumber = 200;
	// scroll index of LineSeries
	private int scrollLineSeriesIndex;
	// index of first shown point in lineSeriesArea (first is on the left side of graph)
	private int firstShownPoint;
	// index of last shown in lineSeriesArea(last is on the right side of graph)
	private int lastShownPoint;
	
	public GraphLineContent(List<GraphLineSeries> series){
		this.pointSeries=series;		
	}
	
	public void draw(Canvas canvas, GraphParams graphParams){
		this.graphParams=graphParams;
		if (pointSeries.isEmpty())
			return;
		//scattersNumber = graphParams.getScattersNumber();
		this.scrollLineSeriesIndex = graphParams.getScrollLineSeriesIndex();
		this.linePointsNumber = graphParams.getLinePointsNumber();
		float sizeBetweenPoints = graphParams.getSizeBetweenPoints();
		int endOfGraph = graphParams.getRightDownCorner().x;

		for (GraphLineSeries series : pointSeries) {
			if(series.isVisible()==false) continue;
			if (series.isEmpty() || series.size() == 1)
				continue;
			int lastIndex = series.size() - 1;
			if ((lastIndex + scrollLineSeriesIndex) >= series.size()) {
				lastIndex = series.size() - 1;
			} else if ((lastIndex + scrollLineSeriesIndex) < 20
					&& scrollLineSeriesIndex != 0 && series.size() > 20)
				lastIndex = 20;
			else if (scrollLineSeriesIndex != 0)
				lastIndex += scrollLineSeriesIndex;
			lastShownPoint = lastIndex;
			firstShownPoint = lastIndex-linePointsNumber;
			graphParams.setEndLineFrame(lastShownPoint);
			graphParams.setStartLineFrame(firstShownPoint);

			Paint lines = new Paint();

			for (int i = 0; i < linePointsNumber; i++) {
				if (i >= lastIndex)
					break;
				//endLineFrame = lastIndex - (i + 1);

				DataPoint point = series.get(lastIndex - i);
				float x0 = Float.valueOf((endOfGraph - i * sizeBetweenPoints));
				float y0 = getGraphYValue(point.getValue());

				point = series.get(lastIndex - (i + 1));
				float x1 = Float.valueOf(endOfGraph - (i + 1)
						* sizeBetweenPoints);
				float y1 = getGraphYValue(point.getValue());

				lines.setColor(series.getColor());
				lines.setStrokeWidth(2);
				canvas.drawLine(x0, y0, x1, y1, lines);
			}
		}		
	}
	
	// return relative Y coordinate in graph
	// param value is absolute in canvas
	private float getGraphYValue(float value) {
		int upBorder = graphParams.getLeftUpCorner().y;
		int downBorder = graphParams.getLeftDownCorner().y;

		float size = downBorder - upBorder;
		float scale = size / 60;
		float absValue = Math.abs(value) - 40;
		float yValue = upBorder + (scale * absValue);
		return yValue;
	}
}
