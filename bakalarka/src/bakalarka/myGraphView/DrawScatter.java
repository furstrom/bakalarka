package bakalarka.myGraphView;

import android.graphics.Canvas;
import android.graphics.Paint;

public class DrawScatter {
	
	public static void drawScatter(Canvas canvas, Paint paint,float x,float y, float min, float max){
		
		paint.setStrokeWidth(2);
		canvas.drawLine(x-15, y, x+15, y, paint); //median
		canvas.drawLine(x-10, min, x+10, min, paint); //min
		canvas.drawLine(x-10, max, x+10, max, paint);//max
		if(min==max && max==y)
			canvas.drawLine(x, y-15, x, y+15, paint);
		
		paint.setStrokeWidth(1);
		canvas.drawLine(x, min, x, max, paint);		
	}
	
	public static void drawScatter(Canvas canvas, Paint paint,float x,float y){
		paint.setStrokeWidth(2);
		canvas.drawLine(x-15, y, x+15, y, paint);
		canvas.drawLine(x, y-15, x, y+15, paint);
		
	}

}
