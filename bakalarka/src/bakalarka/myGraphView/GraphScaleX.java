package bakalarka.myGraphView;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

class GraphScaleX {
	//scaleX values
	String start="0";
	String medium="";
	String border="";
	String end="";
	
	
	public GraphScaleX(){
		
	}
	
	void draw(Canvas canvas,GraphParams params){
		Paint nmbrs = new Paint();
		nmbrs.setColor(Color.WHITE);
		int textSize=40;
		nmbrs.setTextSize(textSize);
		
		Point leftDown=params.getLeftDownCorner();
		Point rightDown=params.getRightDownCorner();
		float scatterAreaSize = params.getScatterAreaSize();
		int borderXPosition=params.getBorderXPosition();		
		
		canvas.drawText(start, leftDown.x-10, leftDown.y+textSize+10, nmbrs);
		canvas.drawText(medium, leftDown.x+scatterAreaSize/2-40, leftDown.y+textSize+10, nmbrs);
		canvas.drawText(border, borderXPosition-40, leftDown.y+textSize+10, nmbrs);
		canvas.drawText(end, rightDown.x-45, rightDown.y+textSize+10, nmbrs);
	}
	
	public void setValue(int i,String value){
		switch (i) {
		case 1:
			start=value;
			break;
		case 2:
			medium=value;
			break;	
		case 3:
			border=value;
			break;
			
		case 4:
			end=value;
			break;

		default:
			break;
		
		}
	}
}
