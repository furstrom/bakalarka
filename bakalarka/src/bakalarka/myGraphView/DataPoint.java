package bakalarka.myGraphView;

import android.os.Parcel;
import android.os.Parcelable;

public class DataPoint implements Parcelable{
	
	private int value;
	
	public DataPoint(int value){
		this.value = value;
	}
	
	public DataPoint(Parcel in){
		this.value = in.readInt();
		
	}
	
	public int getValue(){
		return value;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	/*
	 * This field is needed for Android to be able to
	 * create new objects, individually or as arrays 
	 */
	public static final Parcelable.Creator<DataPoint> CREATOR = 
			new Parcelable.Creator<DataPoint>() {
				@Override
				public DataPoint createFromParcel(Parcel in) {
					return new DataPoint(in);
				}	
				@Override
				public DataPoint[] newArray(int size) {
					return new DataPoint[size];
				}
	};
	
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(value);	
	}
}
