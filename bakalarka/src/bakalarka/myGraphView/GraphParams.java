package bakalarka.myGraphView;

import android.graphics.Canvas;
import android.graphics.Point;

class GraphParams {
	
	Canvas canvas;
	
		//four corners making a graph 
		private Point leftUpCorner = new Point(70,70);
		private Point leftDownCorner;
		private Point rightUpCorner;
		private Point rightDownCorner;
		
		//help values
		private int canvasWidth;
		private int canvasHeight;
		private int graphWidth;
		private int graphHeight;
		private int borderXPosition;
		
		//minimal X position of border
		private int minimalBorder;
		//maximal x border position
		private int maximalBorder;
		//size of area where the graph line is shown
		private float lineAreaSize;
		//how many points in lineArea
		private int linePointsNumber = -1;
		//size between each point
		private float sizeBetweenPoints=-1;
		//size of area where scatters are
		private float scatterAreaSize;
		//size between scatters
		private float sizeBetweenScatters=90;
		//number of scatters in scatterArea
		private int scattersNumber = 5;
		//do you wanna show scatter in scatterArea ?
		private boolean showScatter=false;
		//value for moving with border
		private int moveValue=0;
		//scroll index of LineSeries
		private int scrollLineSeriesIndex;
		//index of first shown point in lineSeriesArea (first is on the left side of graph)
		private int startLineFrame;
		//index of last shown in lineSeriesArea(last is on the right side of graph)
		private int endLineFrame;
		//indicate if min and max in scatter should be shown
		private boolean showMinMaxScatterValues = true;
	
	public GraphParams(){
		linePointsNumber=200;
	}	
		

	public void set(Canvas canvas){
		this.canvas = canvas;

		// set defaut values
		canvasWidth = canvas.getWidth();
		canvasHeight = canvas.getHeight();

		leftUpCorner = new Point(70, 70);
		leftDownCorner = new Point(70, canvasHeight - 70);
		rightUpCorner = new Point(canvasWidth - 50, 70);
		rightDownCorner = new Point(canvasWidth - 50, canvasHeight - 70);

		graphWidth = rightDownCorner.x - leftDownCorner.x;
		graphHeight = leftDownCorner.y - leftUpCorner.y;
		
		minimalBorder = leftUpCorner.x + 100;
		maximalBorder = rightDownCorner.x -200;
		borderXPosition = leftDownCorner.x + (graphWidth * 2 / 3 + moveValue);
		lineAreaSize = rightDownCorner.x - borderXPosition;
		if(linePointsNumber==-1){
			linePointsNumber=200;
		}			
		sizeBetweenPoints = lineAreaSize/(linePointsNumber-1);
		checkScattersNumber(borderXPosition);
		scatterAreaSize = borderXPosition - leftDownCorner.x;		
	}
	
	
	/*moving border
	 * we have to recalculate some values */
	public void moveBorder(int xValue){
		moveValue += xValue;
		int newBorderXPosition = leftDownCorner.x + (graphWidth * 2 / 3 + moveValue);
		checkScattersNumber(newBorderXPosition); //update number of scatters
		if(newBorderXPosition<minimalBorder || newBorderXPosition>maximalBorder){
			moveValue-=xValue;
			return;
		}
		int dBorder = borderXPosition-newBorderXPosition;
		int points = (int)(dBorder/sizeBetweenPoints);
		if(points<0) linePointsNumber +=points;
		else linePointsNumber +=points;		
	}

	public void checkScattersNumber(int borderXPosition){
		int sizeArea = borderXPosition - leftDownCorner.x;
		int val = (int)(Math.round(sizeArea/sizeBetweenScatters))-1;
		this.scattersNumber = Math.max(val, 1);
	}
	
	public Canvas getCanvas() {
		return canvas;
	}

	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
	}
	
	public void setMoveValue(int value){
		this.moveValue=value;
	}
	
	public Point getLeftUpCorner() {
		return leftUpCorner;
	}
	
	public float getSizeBetweenPoints(){
		return sizeBetweenPoints;
	}

	public void setLeftUpCorner(Point leftUpCorner) {
		this.leftUpCorner = leftUpCorner;
	}

	public Point getLeftDownCorner() {
		return leftDownCorner;
	}

	public void setLeftDownCorner(Point leftDownCorner) {
		this.leftDownCorner = leftDownCorner;
	}

	public Point getRightUpCorner() {
		return rightUpCorner;
	}

	public void setRightUpCorner(Point rightUpCorner) {
		this.rightUpCorner = rightUpCorner;
	}

	public Point getRightDownCorner() {
		return rightDownCorner;
	}

	public void setRightDownCorner(Point rightDownCorner) {
		this.rightDownCorner = rightDownCorner;
	}

	public int getCanvasWidth() {
		return canvasWidth;
	}

	public void setCanvasWidth(int canvasWidth) {
		this.canvasWidth = canvasWidth;
	}

	public int getCanvasHeight() {
		return canvasHeight;
	}

	public void setCanvasHeight(int canvasHeight) {
		this.canvasHeight = canvasHeight;
	}

	public int getGraphWidth() {
		return graphWidth;
	}

	public void setGraphWidth(int graphWidth) {
		this.graphWidth = graphWidth;
	}

	public int getGraphHeight() {
		return graphHeight;
	}

	public void setGraphHeight(int graphHeight) {
		this.graphHeight = graphHeight;
	}

	public int getBorderXPosition() {
		return borderXPosition;
	}

	public void setBorderXPosition(int borderXPosition) {
		this.borderXPosition = borderXPosition;
	}

	public float getLineAreaSize() {
		return lineAreaSize;
	}

	public void setLineAreaSize(float lineAreaSize) {
		this.lineAreaSize = lineAreaSize;
	}

	public int getLinePointsNumber() {
		return linePointsNumber;
	}

	public void setLinePointsNumber(int linePointsNumber) {
		this.linePointsNumber = linePointsNumber;
	}

	public float getScatterAreaSize() {
		return scatterAreaSize;
	}

	public float getSizeBetweenScatters() {
		return sizeBetweenScatters;
	}


	public void setScatterAreaSize(float scatterAreaSize) {
		this.scatterAreaSize = scatterAreaSize;
	}

	public int getScattersNumber() {
		return scattersNumber;
	}

	public void setScattersNumber(int scattersNumber) {
		this.scattersNumber = scattersNumber;
	}

	public boolean isShowScatter() {
		return showScatter;
	}

	public void setShowScatter(boolean showScatter) {
		this.showScatter = showScatter;
	}
	
	public void setSizeBetweenLinePoints(int i){
		this.sizeBetweenPoints=i;	
	}
	public void setScrollLineSeriesIndex(int move){
		scrollLineSeriesIndex+=move;
	}
	public int getScrollLineSeriesIndex(){
		return scrollLineSeriesIndex;
	}

	public int getStartLineFrame() {
		return startLineFrame;
	}

	public void setStartLineFrame(int startLineFrame) {
		if(startLineFrame<0) this.startLineFrame = 0;
		else this.startLineFrame = startLineFrame;
	}

	public int getEndLineFrame() {
		return endLineFrame;
	}

	public void setEndLineFrame(int endLineFrame) {
		if(endLineFrame<0) this.endLineFrame=0;
		else this.endLineFrame = endLineFrame;
	}	
	
	
	public boolean isShowMinMaxScatterValues() {
		return showMinMaxScatterValues;
	}

	//if is true set false, if it is false set true
	public void switchScatterMinMaxShowing(){
		boolean value = this.showMinMaxScatterValues;
		showMinMaxScatterValues = !value;
	}
}
