package bakalarka.myGraphView.graphLegend;

import java.util.ArrayList;

import com.example.bakalarka.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MyGraphItemListAdapter extends ArrayAdapter<IGraphLegendItem>{
	private ArrayList<IGraphLegendItem> mListItems;
	private LayoutInflater mLayoutInflater;
	
	public MyGraphItemListAdapter(Context context,ArrayList<IGraphLegendItem> items){
		super(context,0,items);
	
		this.mListItems = items;	
		this.mLayoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);		
	}	
	
	@Override
	public int getCount() {
		return mListItems.size();
	}
	
	@Override
	public IGraphLegendItem getItem(int position) {
		return mListItems.get(position);
	}
	
	@SuppressLint("InflateParams")
	@Override
	public View getView(int position,View convertView,ViewGroup parent){
		View view = convertView;
		
		final IGraphLegendItem listItem = mListItems.get(position);
		if(listItem != null){
			
			GraphLegendItem legendItem = (GraphLegendItem) listItem;
			view = mLayoutInflater.inflate(R.layout.graph_legend_item , null);

			
			final TextView textLegend = (TextView) view.findViewById(R.id.graph_legend_text);
			String text = legendItem.getAsText();
			textLegend.setText(text);
			final int color = legendItem.getColor();
			textLegend.setTextColor(color);	

		}
		return view;		
	}	
}
