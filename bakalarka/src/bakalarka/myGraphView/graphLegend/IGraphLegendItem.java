package bakalarka.myGraphView.graphLegend;

public interface IGraphLegendItem {
	public String getSSID();
	public String gedBSSID();
	public int getFreq();
	public String getSec();
	public boolean isSelected();

}
