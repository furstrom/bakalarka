package bakalarka.myGraphView.graphLegend;

public class GraphLegendItem implements IGraphLegendItem {
	
	private int id=0;
	private String SSID;
	private String BSSID;
	private int frequency;
	private String security;
	private int color;
	private boolean selected;
	
	public GraphLegendItem(int id,String ssid,String bssid,int freq,String sec,int col) {
		this.id = id;
		SSID = ssid;
		BSSID = bssid;
		frequency = freq;
		security = sec;
		color = col;
		selected = true;
	}
	
	@Override
	public String getSSID() {
		return this.SSID;
	}

	@Override
	public String gedBSSID() {
		return this.BSSID;
	}

	@Override
	public int getFreq() {
		return this.frequency;
	}

	@Override
	public String getSec() {
		return this.security;
	}

	@Override
	public boolean isSelected() {
		return this.selected;
	}
	
	public void setSelected(boolean val){
		this.selected = val;
	}
	
	public String getAsText(){
		String freq = Integer.toString(this.frequency);
		String text = id + " SSID: " + this.SSID + ", MAC: " + this.BSSID + 
				", frequency: " + freq + ", secure: " + this.security; 
		return text;
	}
	
	public int getColor(){
		return this.color;		
	}
}
