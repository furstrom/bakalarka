package bakalarka.myGraphView;

import java.util.ArrayList;
import java.util.List;
import android.graphics.Canvas;
import android.graphics.Paint;

public class GraphScattersContent {
	
	private List<float[]> means = new ArrayList<float[]>();
	private List<float[]> mins = new ArrayList<float[]>();
	private List<float[]> maxs = new ArrayList<float[]>();
	private List<GraphScatterSeries> pointSeries;
	private GraphParams graphParams;
	private int endIndex; //index where scatters series ends (star index of line series)
	
	public GraphScattersContent(List<GraphScatterSeries> series){
		this.pointSeries=series;
	}

	public void draw(Canvas canvas, GraphParams graphParams){
		this.graphParams=graphParams;
		if (pointSeries.isEmpty())
			return;
		endIndex = graphParams.getStartLineFrame();	
		int scattersNumber = graphParams.getScattersNumber();
		float xSizeBetweenScatters = graphParams.getSizeBetweenScatters();
		int startOfGraph = graphParams.getLeftDownCorner().x;
		Paint paint = new Paint();

		for (int i = 0; i < pointSeries.size(); i++) {
			if(pointSeries.get(i).isVisible()==false)continue;
			float[] arrayMeans = means.get(i);
			float[] arrayMins = mins.get(i);
			float[] arrayMaxs = maxs.get(i);
			paint.setColor(pointSeries.get(i).getColor());
			for (int j = 0; ((j < scattersNumber)&&(j<arrayMeans.length)); j++) {
				float x = startOfGraph + ((j + 1) * xSizeBetweenScatters);
				float y = getGraphYValue(arrayMeans[j]);
				float min = getGraphYValue(arrayMins[j]);
				float max = getGraphYValue(arrayMaxs[j]);
				if(graphParams.isShowMinMaxScatterValues())
					DrawScatter.drawScatter(canvas, paint, x, y, min, max);
				else DrawScatter.drawScatter(canvas, paint, x, y);
			}
		}	
	}
	
	// recalculate the scatters values
	void updateScatters(int numberScatters) {
		means.clear();
		mins.clear();
		maxs.clear();
		for (GraphScatterSeries series : pointSeries) {
			series.setNumberOfScatters(numberScatters);
			series.setnumberPointInPointList(endIndex);
			means.add(series.getMeans());
			mins.add(series.getMins());
			maxs.add(series.getMaxs());
		}
	}
	
	// return relative Y coordinate in graph
	// param value is absolute in canvas
	private float getGraphYValue(float value) {
		int upBorder = graphParams.getLeftUpCorner().y;
		int downBorder = graphParams.getLeftDownCorner().y;

		float size = downBorder - upBorder;
		float scale = size / 60;
		float absValue = Math.abs(value) - 40;
		float yValue = upBorder + (scale * absValue);
		return yValue;
	}
}
