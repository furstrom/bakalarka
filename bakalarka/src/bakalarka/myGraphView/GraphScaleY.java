package bakalarka.myGraphView;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

class GraphScaleY {
	Canvas canvas;
	int textSize;
	int color;
	String max,onefifth,twofifth,half,fourfifth,fivefifth,min;
	Point leftUp,rightDown;
	int graphHeight;
	
	
	
	
	public GraphScaleY(Canvas canvas,GraphParams params){
		this.canvas=canvas;
		textSize=40;
		color = Color.WHITE;
		max = "-40";
		onefifth="-50";
		twofifth="-60";
		half="-70";
		fourfifth="-80";
		fivefifth="-90";
		min="-100";
		leftUp=params.getLeftUpCorner();
		rightDown=params.getRightDownCorner();
		graphHeight=params.getGraphHeight();
				
		
	}
	
	public void draw(){
		Paint nmbrs = new Paint();
		nmbrs.setColor(color);
		nmbrs.setTextSize(textSize);
		canvas.drawText(max, 5, leftUp.y+textSize/3, nmbrs);
		canvas.drawText(onefifth, 5, leftUp.y+graphHeight/6+textSize/3, nmbrs);
		canvas.drawText(twofifth, 5, leftUp.y+graphHeight*2/6+textSize/3, nmbrs);
		canvas.drawText(half, 5, leftUp.y+graphHeight/2+textSize/3, nmbrs);
		canvas.drawText(fourfifth, 5, leftUp.y+graphHeight*4/6+textSize/3, nmbrs);
		canvas.drawText(fivefifth, 5, leftUp.y+graphHeight*5/6+textSize/3, nmbrs);
		nmbrs.setTextSize(textSize-5);
		canvas.drawText(min, 1, rightDown.y+textSize/3, nmbrs);
	}

}
