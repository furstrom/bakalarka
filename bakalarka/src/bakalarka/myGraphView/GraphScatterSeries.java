package bakalarka.myGraphView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.math3.stat.descriptive.rank.Percentile;

import android.graphics.Color;

public class GraphScatterSeries implements List<DataPoint>{
	private String name;
	private int color;
	private int scatters;
	private ArrayList<DataPoint> dataList;
	//number of points in LineSeries part which we dont want include in ScatterSeries
	private int numberPointInPointList;
	private boolean visible;
	
	public GraphScatterSeries() {
		long time = System.currentTimeMillis();
		name = "GraphScatterSeries" + Long.toString(time);
		color = Color.WHITE;
		scatters=0;
		dataList = new ArrayList<DataPoint>();
		numberPointInPointList=200;
		visible = true;
	}
	
	public GraphScatterSeries(String name,int color,int scatters){
		this.name = name;
		this.color = color;
		this.scatters = scatters;
		dataList = new ArrayList<DataPoint>();
		numberPointInPointList=200;
		visible = true;
	}
	
	public boolean isVisible(){
		return this.visible;
	}
	
	public void setVisible(boolean val){
		this.visible = val;
	}
	
	public void setColor(int color){
		this.color = color;
	}
	
	public int getColor(){
		return color;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public void setNumberOfScatters(int nmbr){
		synchronized (this) {
			scatters=nmbr;
		}
	}
	public int getNumberOfScatters(){
		synchronized (this) {
			return scatters;
		}
	}
	
	public float[] getMeans(){
		synchronized (this) {
			float[] medians = new float[scatters];
			//int count = 0;
			int i = 0;
			List<List<DataPoint>> subLists = devideDataList();
			for (List<DataPoint> subList : subLists) {
				if (i >= scatters) break;
				//count = 0;	
				
				int[] array = toIntArray(subList);
				Arrays.sort(array);
				int median;
				if(array.length % 2 == 0)
					median = (int)Math.round(((double)array[array.length/2] + (double)array[array.length/2 - 1])/2);
				else
					 median = array[array.length/2];
				medians[i] = median;
				
				i++;
			}
			return medians;
		}
	}
	
	public float[] getMins(){
		synchronized (this) {
			float[] mins = new float[scatters];
			int i = 0;
			List<List<DataPoint>> subLists = devideDataList();
			for (List<DataPoint> subList : subLists) {
				double[] array = toDoubleArray(subList);
				Arrays.sort(array);
				int per = (int) new Percentile().evaluate(array, 5);
				mins[i] = per;
				i++;
			}
			return mins;
		}
	}
	
	public float[] getMaxs(){
		synchronized (this) {
			float[] maxs = new float[scatters];
			int i = 0;
			List<List<DataPoint>> subLists = devideDataList();
			for (List<DataPoint> subList : subLists) {
				double[] array = toDoubleArray(subList);
				Arrays.sort(array);
				int per = (int) new Percentile().evaluate(array, 95);
				maxs[i] = per;
				i++;
			}
			return maxs;
		}
	}
	
	private List<List<DataPoint>>devideDataList(){
		synchronized (this) {
			List<List<DataPoint>> partsList = new ArrayList<List<DataPoint>>();
			List<DataPoint> pointList = dataList.subList(0,numberPointInPointList);
			int partSize = (int) Math.ceil((double) pointList.size() / (double) scatters);
			for (int start = 0; start < pointList.size(); start += partSize) {
				int end = Math.min(start + partSize, pointList.size());
				List<DataPoint> subList = pointList.subList(start, end);
				partsList.add(subList);
			}
			return partsList;
		}		
	}
	
	public void setnumberPointInPointList(int nmbr){
		synchronized (this) {
			this.numberPointInPointList = nmbr;
		}
	}

	@Override
	public boolean add(DataPoint dataPoint) {
		synchronized (this) {
			return dataList.add(dataPoint);
		}
	}

	@Override
	public void add(int index, DataPoint dataPoint) {
		synchronized (this) {
			dataList.add(index, dataPoint);
		}	
	}

	@Override
	public boolean addAll(Collection<? extends DataPoint> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(int arg0, Collection<? extends DataPoint> arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean contains(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public DataPoint get(int index) {
		synchronized (this) {
			return dataList.get(index);
		}
	}

	@Override
	public int indexOf(Object dataPoint) {
		return dataList.indexOf(dataPoint);
	}

	@Override
	public boolean isEmpty() {
		synchronized (this) {
			return dataList.isEmpty();
		}
	}

	@Override
	public Iterator<DataPoint> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int lastIndexOf(Object arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ListIterator<DataPoint> listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListIterator<DataPoint> listIterator(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DataPoint remove(int index) {
		return dataList.remove(index);
	}

	@Override
	public boolean remove(Object dataPoint) {
		return dataList.remove(dataPoint);
	}

	@Override
	public boolean removeAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public DataPoint set(int index, DataPoint dataPoint) {
		return dataList.set(index, dataPoint);
	}

	@Override
	public int size() {
		synchronized (this) {
			return dataList.size();
		}
		
	}

	@Override
	public List<DataPoint> subList(int start, int end) {
		synchronized (this) {
			return dataList.subList(start, end);
		}
	}

	@Override
	public Object[] toArray() {
		return dataList.toArray();
	}

	@Override
	public <T> T[] toArray(T[] dataArray) {
		return dataList.toArray(dataArray);
	}
	
	//returns array of integer DataPont's value
	public int[] toIntArray(){
		int[] array = new int[dataList.size()];
		for(int i=0;i<array.length;i++){
			array[i] = dataList.get(i).getValue();
		}
		return array;
	}
	private int[] toIntArray(List<DataPoint> list){
		int[] array = new int[list.size()];
		for(int i=0;i<array.length;i++){
			array[i] = list.get(i).getValue();
		}
		return array;
	}
	private double[] toDoubleArray(List<DataPoint> list){
		double[] array = new double[list.size()];
		for(int i=0;i<array.length;i++){
			array[i] = list.get(i).getValue();
		}
		return array;
	}
}

